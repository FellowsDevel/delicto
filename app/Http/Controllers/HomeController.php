<?php

namespace Delicto\Http\Controllers;

use Delicto\Http\Requests;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//    return view( 'layouts.app' );
        return view('layouts.agenda');
    }

}
