<?php

namespace Delicto\Http\Controllers;

use Delicto\TipoObjeto;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TipoObjetoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Exibe a lista de Grupos
     *
     * @return Response
     */
    public function index()
    {
        $lista = DB::table('tipo_objetos')->paginate(QTD_REGISRTOS_PAGINACAO);
        return view('tipo_objeto.index')->with('registros', $lista);
    }

    /**
     * Exibe o formulário para criar novo registro
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_objeto.create');
    }

    /**
     * Guarda o registro no BD.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'descricao' => 'required'
        ]);

        $reg = new TipoObjeto;
        $reg->descricao = $request->descricao;
        $reg->save();

        return redirect()
            ->action('TipoObjetoController@index')
            ->withInput(['descricao' => $request->descricao]);
    }

    /**
     * Exibe o formulário para edição do registro.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $tipo = TipoObjeto::find($id);
        return view('tipo_objeto.edit')->with('tipo_objeto', $tipo);
    }

    /**
     * Atualiza o registro no BD.
     *
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'descricao' => 'required'
        ]);

        $tipo_objeto = TipoObjeto::find($id);
        $anterior = $tipo_objeto->descricao;
        $tipo_objeto->descricao = $request->descricao;
        $tipo_objeto->save();

        return redirect()
            ->action('TipoObjetoController@index')
            ->withInput(['alterado' => $request->descricao,
                'anterior' => $anterior]);
    }

    /**
     * Exclui o registro do BD.
     *
     * @param  TipoObjeto $tipo_objeto
     * @return Response
     */
    public function destroy(TipoObjeto $tipo_objeto)
    {

        try {
            $tipo_objeto->delete();
            return redirect('/tipo_objeto');
        } catch (QueryException $e) {

            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Este Tipo de Objeto não pode ser removido pois o mesmo está sendo utilizado';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }

}
