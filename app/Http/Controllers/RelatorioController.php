<?php

namespace Delicto\Http\Controllers;

use Delicto\Abertura;
use Delicto\DataExtenso;
use Delicto\Http\Requests;
use Delicto\Qualificacao;
use Log;

class RelatorioController extends Controller
{
    private function montaTextoCondutor($qualificacao)
    {
        $texto_condutor = "";
        $texto_condutor .= $qualificacao['pessoa']['nome'];

        if ($qualificacao['pessoa']['nacionalidade']) {
            $texto_condutor .= ', ' . $qualificacao['pessoa']['nacionalidade'];
        }

        if ($qualificacao['pessoa']['naturalidade']) {
            $texto_condutor .= ', ' . 'natural de ' . $qualificacao['pessoa']['naturalidade'];
        }

        if ($qualificacao['pessoa']['matricula']) {
            $texto_condutor .= ', ' . 'matricula ' . $qualificacao['pessoa']['matricula'];
        }

        return $texto_condutor;
    }

    private function getAsStringSeparadaPorVirgula($lista)
    {
        $nomes_pontuados = '';
        $len = sizeof($lista);
        for ($x = 0; $x < $len; $x++) {
            $nomes_pontuados .= $lista[$x];
            if ($x == $len - 2) {
                $nomes_pontuados .= ' e ';
            } else if ($len > 1 && $x < $len - 2) {
                $nomes_pontuados .= ', ';
            }
        }
        return $nomes_pontuados;
    }

    public function rel_apdf($abertura_id)
    {
        $abertura = Abertura::findOrFail($abertura_id);
        $this->authorize('abertura', $abertura);

        $hora = substr($abertura->hora, 0, 2);
        $minuto = substr($abertura->hora, 3, 5);
        $hora_extenso = trim(DataExtenso::valorPorExtenso($hora));
        $minuto_extenso = trim(DataExtenso::valorPorExtenso($minuto));

        $data_extenso = trim(DataExtenso::dataPorExtenso($abertura->data));

        $estados = Utils::getEstados();
        $cidades = Utils::getCidade($abertura->abertura_estado);
        $estado = $estados[$abertura->abertura_estado];
        $cidade = $cidades[$abertura->abertura_cidade];

        $delegado = $abertura->delegado | '';
        $circunscricao = $abertura->circunscricao | '';

        $autuados = Qualificacao::with('pessoa', 'ouvida')->where(['abertura_id' => $abertura_id, 'tipo_pessoa_id' => 1])->get(); // tipo_pessoa_id = 1 -> Autuado
        $condutores = Qualificacao::with('pessoa', 'ouvida')->where(['abertura_id' => $abertura_id, 'tipo_pessoa_id' => 2])->get(); // tipo_pessoa_id = 2 -> Condutor
        $testemunhas = Qualificacao::with('pessoa', 'ouvida')->where(['abertura_id' => $abertura_id, 'tipo_pessoa_id' => 5])->get(); // tipo_pessoa_id = 5 -> Testemunha


        $condutor_array = $condutores->toArray()[0];
        $texto_condutor = self::montaTextoCondutor($condutor_array);
        $ouvida_condutor = $condutor_array['ouvida']['ouvida'];

        $nomes_autuados = $autuados->pluck('pessoa.nome')->toArray();
        $nomes_autuados_pontuados = $this->getAsStringSeparadaPorVirgula($nomes_autuados);

        $nomes_testemunhas = $testemunhas->pluck('pessoa.nome')->toArray();
        $nomes_testemunhas_pontuados = $this->getAsStringSeparadaPorVirgula($nomes_testemunhas);

        $texto = "Às $hora_extenso horas e $minuto_extenso minutos de $data_extenso nesta cidade de $cidade, do Estado de $estado e no Cartório desta $circunscricao, " .
            "onde presente se encontrava $delegado, respectivo Delegado de Polícia Civil, comigo Escrivão(ã) de seu cargo no final assinado, " .
            "ai compareceu a pessoa na qualidade de CONDUTOR: $texto_condutor. Aos costumes, disse nada. Compromissado na forma da lei,  " .
            "advertido das penas cominadas ao falso testemunho, prometeu dizer a verdade do que soubesse e lhe fosse perguntado. " .
            "Inquirido, respondeu: QUE apresenta preso em flagrante delito: $nomes_autuados_pontuados, na presença da(s) TESTEMUNHA(S): $nomes_testemunhas_pontuados, " .
            "disse QUE '$ouvida_condutor'. Nada mais havendo, lido e achado conforme, determinou a Autoridade que fosse encerrado o presente termo, " .
            "o qual assina juntamente com o Depoente e comigo Escrivão(ã), que o digitei.";


        $nome_relatorio = 'apfd';
        $parametros_relatorio = array(
            "texto" => $texto,
            "caminho" => dirname(__FILE__) . '/../../phpjasperxml',
        );

        include(dirname(__FILE__) . '/../../phpjasperxml/relatorio.php');


    }
}
