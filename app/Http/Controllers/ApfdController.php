<?php

namespace Delicto\Http\Controllers;

use Delicto\Abertura;
use Delicto\ApfdRelatorio;
use Delicto\AvaliacaoIndireta;
use Delicto\Despacho;
use Delicto\Fianca;
use Delicto\TipoObjeto;
use Delicto\TipoPessoa;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;

class ApfdController extends AberturaController
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     * Só habilita o icone de
     * @param $quals
     * @return bool
     */
    private function habilitaImpressaoApfd($quals)
    {
        foreach ($quals as $is => $vs) {

            $tipos = collect($vs->qualificacoes)->pluck('tipo_pessoa_id')->toArray();
            // 1 - Autuado
            // 2 - Condutor
            // 5 - Testemunha
            // 6 - Vitima

            if (in_array('1', $tipos) && in_array('2', $tipos) && in_array('5', $tipos)
                && in_array('6', $tipos)
            ) {
                $vs['habilita_impressao'] = true;
            } else {
                $vs['habilita_impressao'] = false;
            }
        }
    }

    /**
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->id === 2) { // administrador
            $lista = Abertura::with('qualificacoes.pessoa')
                ->where(['tipo_registro' => 'APFD'])
                ->paginate(QTD_REGISRTOS_PAGINACAO);

        } else {
            $lista = Abertura::with('qualificacoes.pessoa')
                ->where(['user_id' => Auth::user()->id, 'tipo_registro' => 'APFD'])
                ->paginate(QTD_REGISRTOS_PAGINACAO);
        }

        $this->habilitaImpressaoApfd($lista);

        return view('apfd.index')
            ->with('registros', $lista);
    }

    /**
     * Exibe o formulário para criar novo registro
     *
     * @return Response
     */
    public function create()
    {
        $tiposPessoa = TipoPessoa::orderBy('descricao')->lists('descricao', 'id');
        $tiposObjeto = TipoObjeto::orderBy('descricao')->lists('descricao', 'id');
        $estados = Utils::getEstados();

        return view('apfd.create')
            ->with('tiposObjeto', $tiposObjeto)
            ->with('estados', $estados)
            ->with('cidades', ["Selecione"])
            ->with('despacho', ["id" => "", "despacho" => ""])
            ->with('fianca', ["id" => "", "fianca" => ""])
            ->with('avaliacao', '')
            ->with('abertura', '')
            ->with('apfdrelatorio', ['id' => '', 'relatorio' => ''])
            ->with('tiposPessoa', $tiposPessoa);
    }

    /**
     * Guarda o registro no BD.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'delegado' => 'required',
            'circunscricao' => 'required'
        ]);

        $abertura = new Abertura;
        $input = $request->all();

        $abertura->user_id = Auth::id(); //O criador do APFD é o usuário logado
        $abertura->fill($input);
        $abertura->tipo_registro = 'APFD';
        $abertura->save();

        $despacho = new Despacho;
        $despacho->abertura_id = $abertura->id;
        $despacho->save();

        $fianca = new Fianca;
        $fianca->abertura_id = $abertura->id;
        $fianca->save();

        $avaliacao = new AvaliacaoIndireta;
        $avaliacao->abertura_id = $abertura->id;
        $avaliacao->save();

        $apfd_relatorio = new ApfdRelatorio;
        $apfd_relatorio->abertura_id = $abertura->id;
        $apfd_relatorio->save();

        return redirect()->action('ApfdController@edit', $abertura->id);
    }

    /**
     * Exibe o formulário para edição do registro.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $abertura = Abertura::with('despacho', 'fianca', 'avaliacao', 'apfdrelatorio')->findOrFail($id);
        $this->authorize('abertura', $abertura);

        $arrAbertura = $abertura->toArray();
        $despacho = $arrAbertura['despacho'];
        $fianca = $arrAbertura['fianca'];
        $avaliacao = $arrAbertura['avaliacao'];
        $apfdrelatorio = $arrAbertura['apfdrelatorio'];

        $tiposPessoa = TipoPessoa::orderBy('descricao')->lists('descricao', 'id');
        $tiposObjeto = TipoObjeto::orderBy('descricao')->lists('descricao', 'id');
        $estados = Utils::getEstados();


        $cidades = Utils::getCidade($abertura->abertura_estado);
        if (!$cidades) {
            $cidades = ['' => 'Selecione'];
        }

        return view('apfd.edit')
            ->with('tiposPessoa', $tiposPessoa)
            ->with('tiposObjeto', $tiposObjeto)
            ->with('estados', $estados)
            ->with('cidades', $cidades)
            ->with('despacho', $despacho)
            ->with('fianca', $fianca)
            ->with('avaliacao', $avaliacao)
            ->with('apfdrelatorio', $apfdrelatorio)
            ->with('abertura', $abertura);
    }

    /**
     * Atualiza o registro no BD.
     *
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'delegado' => 'required',
            'circunscricao' => 'required'
        ]);

        $abertura = Abertura::findOrFail($id);
        $this->authorize('abertura', $abertura);
        $input = $request->all();
        $abertura->fill($input)->save();

        return redirect()->action('ApfdController@index');
    }


    public function destroy(Request $request)
    {

        try {
            $ab = Abertura::findOrFail($request->id);
            $this->authorize('abertura', $ab);

            $ab->delete();
            return redirect('/apfd');
        } catch (QueryException $e) {

            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Não pode apagar este APFD porque possui dependências';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }


}
