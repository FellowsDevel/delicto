<?php

namespace Delicto\Http\Controllers;

use Delicto\Funcao;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FuncaoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        DB::disableQueryLog();
    }

    /**
     * Exibe a lista de Grupos
     *
     * @return Response
     */
    public function index()
    {
        $registros = DB::table('funcoes')->paginate(QTD_REGISRTOS_PAGINACAO);
        return view('funcao.index')->with('registros', $registros);
    }

    /**
     * Exibe o formulário para criar novo registro
     *
     * @return Response
     */
    public function create()
    {
        return view('funcao.create');
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'descricao' => 'required'
        ]);

        $funcao = new Funcao;
        $funcao->descricao = $request->descricao;
        $funcao->save();

        return redirect()
            ->action('FuncaoController@index')
            ->withInput(['descricao' => $funcao->descricao]);
    }

    /**
     * Exibe o formulário para edição do registro.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $funcao = Funcao::find($id);
        return view('funcao.edit')->with('funcao', $funcao);
    }

    /**
     * @param Request $req
     * @param $id
     * @return $this
     */
    public function update(Request $req, $id)
    {
        $this->validate($req, [
            'descricao' => 'required'
        ]);

        $funcao = Funcao::find($id);
        $anterior = $funcao->descricao;
        $funcao->descricao = $req->descricao;
        $funcao->save();

        return redirect()
            ->action('FuncaoController@index')
            ->withInput(['alterado' => $req->descricao,
                'anterior' => $anterior]);
    }


    public function destroy(Funcao $funcao)
    {
        try {
            $funcao->delete();
            return redirect('/funcao');
        } catch (QueryException $e) {

            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Esta Função não pode ser removida pois a mesma está sendo utilizada';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }

}
