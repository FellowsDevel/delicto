<?php

namespace Delicto\Http\Controllers;

use Delicto\Abertura;
use Delicto\Pessoa;
use Delicto\Qualificacao;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Log;

class QualificacaoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Retorna a lista de Qualificações de um APFD como um objeto JSON
     *
     * @return Response
     */
    public function index($id)
    {
        $abertura = Abertura::findOrFail($id);
        $this->authorize('abertura', $abertura);

        $regs = Qualificacao::with('tipoPessoa', 'pessoa')
            ->where('abertura_id', $id)->get();

        return $regs;
    }

    /**
     * Guarda o registro no BD.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'abertura_id' => 'required',
            'tipo_pessoa_id' => 'required',
            'nome' => 'required'
        ]);

        $abertura = Abertura::findOrFail($request->abertura_id);
        $this->authorize('abertura', $abertura);

        $pessoa = new Pessoa;
        $pessoa->nome = $request->nome;
        $pessoa->apelido = $request->apelido;
        $pessoa->rg = $request->rg;
        $pessoa->cpf = $request->cpf;
        $pessoa->ocupacao = $request->ocupacao;
        $pessoa->matricula = $request->matricula;
        $pessoa->local_trabalho = $request->local_trabalho;
        $pessoa->estado_civil = $request->estado_civil;
        $pessoa->nacionalidade = $request->nacionalidade;
        $pessoa->naturalidade = $request->naturalidade;
        $pessoa->data_nascimento = $request->data_nascimento;
        $pessoa->idade = $request->idade;
        $pessoa->nome_mae = $request->nome_mae;
        $pessoa->nome_pai = $request->nome_pai;
        $pessoa->escolaridade = $request->escolaridade;
        $pessoa->sexo = $request->sexo;
        $pessoa->cep = $request->cep;
        $pessoa->logradouro = $request->logradouro;
        $pessoa->numero = $request->numero;
        $pessoa->complemento = $request->complemento;
        $pessoa->referencia = $request->referencia;
        $pessoa->bairro = $request->bairro;
        $pessoa->cidade = $request->cidade;
        $pessoa->uf = $request->uf;
        $pessoa->telefone_1 = $request->telefone_1;
        $pessoa->telefone_2 = $request->telefone_2;
        $pessoa->advogado = $request->advogado;
        $pessoa->save();

        $qualif = new Qualificacao;
        $qualif->tipo_pessoa_id = $request->tipo_pessoa_id;
        $qualif->pessoa_id = $pessoa->id;
        $qualif->abertura_id = $request->abertura_id;
        $qualif->tipificacao = $request->tipificacao;

        $qualif->save();

        return $qualif;
    }

    /*
     * Exibe o registro.
     */

    public function show($id)
    {
        $reg = Qualificacao::with('tipoPessoa', 'pessoa')->where('id', $id)->get();
        $quali = json_decode($reg);

        $abertura = Abertura::findOrFail($quali[0]->abertura_id);
        $this->authorize('abertura', $abertura);

        return $reg;
    }

    /*
     * Atualiza o registro no BD.
     */

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'abertura_id' => 'required',
            'tipo_pessoa_id' => 'required',
            'pessoa_id' => 'required',
            'nome' => 'required'
        ]);

        $qualif = Qualificacao::findOrFail($id);
        $abertura = Abertura::findOrFail($qualif->abertura_id);
        $this->authorize('abertura', $abertura);

        $pessoa = Pessoa::findOrFail($request->pessoa_id);
        $pessoa->nome = $request->nome;
        $pessoa->apelido = $request->apelido;
        $pessoa->rg = $request->rg;
        $pessoa->cpf = $request->cpf;
        $pessoa->ocupacao = $request->ocupacao;
        $pessoa->matricula = $request->matricula;
        $pessoa->local_trabalho = $request->local_trabalho;
        $pessoa->estado_civil = $request->estado_civil;
        $pessoa->nacionalidade = $request->nacionalidade;
        $pessoa->naturalidade = $request->naturalidade;
        $pessoa->data_nascimento = $request->data_nascimento;
        $pessoa->idade = $request->idade;
        $pessoa->nome_mae = $request->nome_mae;
        $pessoa->nome_pai = $request->nome_pai;
        $pessoa->escolaridade = $request->escolaridade;
        $pessoa->sexo = $request->sexo;
        $pessoa->cep = $request->cep;
        $pessoa->logradouro = $request->logradouro;
        $pessoa->numero = $request->numero;
        $pessoa->complemento = $request->complemento;
        $pessoa->referencia = $request->referencia;
        $pessoa->bairro = $request->bairro;
        $pessoa->cidade = $request->cidade;
        $pessoa->uf = $request->uf;
        $pessoa->telefone_1 = $request->telefone_1;
        $pessoa->telefone_2 = $request->telefone_2;
        $pessoa->advogado = $request->advogado;
        $pessoa->save();


        $qualif->tipo_pessoa_id = $request->tipo_pessoa_id;
        $qualif->pessoa_id = $pessoa->id;
        $qualif->abertura_id = $request->abertura_id;
        $qualif->tipificacao = $request->tipificacao;
        $qualif->save();

        return $qualif;
    }

    /*
     * Exclui o registro do BD.
     */

    public function destroy($id)
    {
        try {
            $reg = Qualificacao::findOrFail($id);
            $abertura = Abertura::findOrFail($reg->abertura_id);
            $this->authorize('abertura', $abertura);
            $reg->delete();
        } catch (QueryException $e) {

            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Não é possível apagar esta Qualificação porque existe Ouvida ou Objetos dessa pessoa';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }

}
