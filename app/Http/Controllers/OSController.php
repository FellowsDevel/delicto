<?php

namespace Delicto\Http\Controllers;

use Delicto\Abertura;
use Delicto\Http\Requests;
use Delicto\OrdemServico;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Log;

class OSController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index($id)
    {

        $abertura = Abertura::findOrFail($id);
        $this->authorize('abertura', $abertura);

        $regs = OrdemServico::where('abertura_id', $id)->orderBy('sequencial', 'asc')->get();
        return $regs;

    }


    public function store(Request $request)
    {

        $this->validate($request, [
            'abertura_id' => 'required',
            'sequencial' => 'required',
            'data_os' => 'required',
            'diligencia' => 'required'
        ]);

        $abertura = Abertura::findOrFail($request->abertura_id);
        $this->authorize('abertura', $abertura);

        $reg = new OrdemServico;
        $reg->abertura_id = $request->abertura_id;
        $reg->sequencial = $request->sequencial;
        $reg->data_os = $request->data_os;
        $reg->diligencia = $request->diligencia;
        $reg->save();

        return $reg;
    }

    /**
     * Exibe o registro.
     * @param type $id
     * @return type
     */
    public function show($id)
    {

        $reg = OrdemServico::where('id', $id)->get();

        $r = json_decode($reg);
        $abertura = Abertura::findOrFail($r[0]->abertura_id);
        $this->authorize('abertura', $abertura);

        return $reg;
    }

    /**
     * Atualiza o registro no BD.
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function update(Request $request, $id)
    {

        $reg = OrdemServico::findOrFail($id);
        $abertura = Abertura::findOrFail($reg->abertura_id);
        $this->authorize('abertura', $abertura);

        $input = $request->all();

        $reg->fill($input)->save();

        return $reg;
    }


    public function destroy($id)
    {

        try {
            $reg = OrdemServico::findOrFail($id);
            $abertura = Abertura::findOrFail($reg->abertura_id);
            $this->authorize('abertura', $abertura);
            $reg->delete();
        } catch (QueryException $e) {

            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Não é possível apagar esta Ordem de Serviço porque existe dependencias';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }

}
