<?php

namespace Delicto\Http\Controllers;

use Delicto\Abertura;
use Delicto\Ouvida;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Log;

class OuvidaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Retorna a lista como um objeto JSON
     *
     * @return Response
     */
    public function index($id)
    {

        $abertura = Abertura::findOrFail($id);
        $this->authorize('abertura', $abertura);

        $regs = Ouvida::with('qualificacao.tipoPessoa', 'qualificacao.pessoa')
            ->where('abertura_id', $id)->orderBy('data_ouvida', 'desc')->get();
        return $regs;
    }

    /**
     * Retorna a lista como um objeto JSON
     * @param $abertura_id
     * @return mixed
     */
    public function ouvidasAbertas($abertura_id)
    {

        $abertura = Abertura::findOrFail($abertura_id);
        $this->authorize('abertura', $abertura);

        $regs = \Delicto\Qualificacao::with('tipoPessoa', 'pessoa')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('ouvidas')
                    ->whereRaw('qualificacoes.id = ouvidas.qualificacao_id');
            })->where('abertura_id', $abertura_id)->get();
        return $regs;
    }

    /**
     * Guarda o registro no BD e retorna o registro salvo
     *
     * @param Request $request
     * @return Ouvida
     */
    public function store(Request $request)
    {

        $abertura = Abertura::findOrFail($request->abertura_id);
        $this->authorize('abertura', $abertura);

        $reg = new Ouvida;
        $input = $request->all();
        $reg->fill($input)->save();

        return $reg;
    }

    /**
     * Exibe o registro.
     * @param type $id
     * @return type
     */
    public function show($id)
    {

        $reg = Ouvida::with('qualificacao.tipoPessoa', 'qualificacao.pessoa')->where('id', $id)->get();

        $r = json_decode($reg);
        $abertura = Abertura::findOrFail($r[0]->abertura_id);
        $this->authorize('abertura', $abertura);

        return $reg;
    }

    /**
     * Atualiza o registro no BD.
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function update(Request $request, $id)
    {
//    $this->validate( $request, [
//      'ouvida' => 'required'
//    ] );

        $reg = Ouvida::findOrFail($id);
        $abertura = Abertura::findOrFail($reg->abertura_id);
        $this->authorize('abertura', $abertura);

        $input = $request->all();

        $reg->fill($input)->save();

        return $reg;
    }

    /**
     * Exclui o registro do BD.
     * @param type $id
     * @return type
     */
    public function destroy($id)
    {
        try {
            $reg = Ouvida::findOrFail($id);
            $abertura = Abertura::findOrFail($reg->abertura_id);
            $this->authorize('abertura', $abertura);
            $reg->delete();
        } catch (QueryException $e) {

            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Não é possível apagar esta Ouvida porque existe(m) Objeto(s) relacionado(s)';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }

}
