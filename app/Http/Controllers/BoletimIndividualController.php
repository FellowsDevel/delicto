<?php

namespace Delicto\Http\Controllers;

use Delicto\Abertura;
use Delicto\BoletimIndividual;
use Delicto\Qualificacao;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Log;

class BoletimIndividualController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function listaAutuados($abertura_id)
    {
        $abertura = Abertura::findOrFail($abertura_id);
        $this->authorize('abertura', $abertura);

        $regs = \Delicto\Qualificacao::with('pessoa')
            ->where(['abertura_id' => $abertura_id])
            ->where(['tipo_pessoa_id' => 1])/* tipo de pessoa: Autuado */
            ->get();

        return $regs;
    }


    public function restante($abertura_id)
    {

        $abertura = Abertura::findOrFail($abertura_id);
        $this->authorize('abertura', $abertura);

        $regs = Qualificacao::whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('boletins_individuais')
                ->whereRaw('qualificacoes.id = boletins_individuais.qualificacao_id and qualificacoes.abertura_id = boletins_individuais.abertura_id');
        })
            ->with('pessoa')
            ->where(['abertura_id' => $abertura_id, 'tipo_pessoa_id' => 1])// autuados apenas
            ->get();


        return $regs;
    }


    public function index($id)
    {
        $abertura = Abertura::findOrFail($id);
        $this->authorize('abertura', $abertura);

        $regs = BoletimIndividual::with('qualificacao.tipoPessoa', 'qualificacao.pessoa')
            ->where('abertura_id', $id)->get();

        return $regs;
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'abertura_id' => 'required',
            'qualificacao_id' => 'required'
        ]);

        $abertura = Abertura::findOrFail($request->abertura_id);
        $this->authorize('abertura', $abertura);

        $boletim = new BoletimIndividual;
        $input = $request->all();
        $boletim->fill($input)->save();

        return $boletim;
    }


    public function show($id)
    {
        $reg = BoletimIndividual::with('qualificacao.tipoPessoa', 'qualificacao.pessoa')->where('id', $id)->get();
        $boletim = json_decode($reg);

        $abertura = Abertura::findOrFail($boletim[0]->abertura_id);
        $this->authorize('abertura', $abertura);

        return $reg;
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'abertura_id' => 'required',
            'qualificacao_id' => 'required'
        ]);

        $boletim = BoletimIndividual::findOrFail($id);
        $abertura = Abertura::findOrFail($boletim->abertura_id);
        $this->authorize('abertura', $abertura);

        $boletim->abertura_id = $request->abertura_id;

        $qual = Qualificacao::find($request->qualificacao_id);
        $boletim->qualificacao()->associate($qual);

        $boletim->uf_boletim = $request->uf_boletim;
        $boletim->distrito = $request->distrito;
        $boletim->zona = $request->zona;
        $boletim->data = $request->data;
        $boletim->periodo = $request->periodo;
        $boletim->praticado = $request->praticado;
        $boletim->lugar_ocorrencia = $request->lugar_ocorrencia;
        $boletim->meio_empregado = $request->meio_empregado;
        $boletim->motivos_presumiveis = $request->motivos_presumiveis;
        $boletim->infracao_prevista = $request->infracao_prevista;
        $boletim->preso = $request->preso;
        $boletim->data_inquerito = $request->data_inquerito;
        $boletim->save();

        return $boletim;
    }

    public function destroy($id)
    {
        try {
            $reg = BoletimIndividual::findOrFail($id);
            $abertura = Abertura::findOrFail($reg->abertura_id);
            $this->authorize('abertura', $abertura);
            $reg->delete();
        } catch (QueryException $e) {

            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Não é possível apagar este Boletim Individual porque existe dependencias';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }

}
