<?php

namespace Delicto\Http\Controllers;

use Delicto\Abertura;
use Delicto\Pericia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PericiaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Retorna a lista como um objeto JSON
     *
     * @return Response
     */
    public function index($id)
    {
        $abertura = Abertura::findOrFail($id);
        $this->authorize('abertura', $abertura);

        $regs = Pericia::with('objeto', 'tipoObjeto')
            ->where('abertura_id', $id)->get();

        return $regs;
    }

    /**
     * Retorna a lista como um objeto JSON
     * @param $abertura_id
     * @return mixed
     */
    public function restante($abertura_id)
    {

        $abertura = Abertura::findOrFail($abertura_id);
        $this->authorize('abertura', $abertura);

        $regs = DB::select(DB::raw(
            "SELECT * 
       FROM   objetos 
       WHERE  not exists( select 1 from pericias where objetos.id = pericias.objeto_id and objetos.abertura_id = pericias.abertura_id )        
       AND    abertura_id = " . $abertura_id
        ));

        return $regs;
    }


    /**
     * Guarda o registro no BD.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tipo_objeto_id' => 'required',
            'objeto_id' => 'required'
        ]);

        $reg = new Pericia;
        $input = $request->all();
        $reg->fill($input)->save();

        return $reg;
    }

    /*
     * Exibe o registro.
     */

    public function show($id)
    {

        $reg = Pericia::findOrFail($id);
//    $abertura = Abertura::findOrFail($reg->abertura_id);
//    $this->authorize('abertura', $abertura);

        return $reg;
    }

    /*
     * Atualiza o registro no BD.
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tipo_objeto_id' => 'required',
            'pericia' => 'required'
        ]);

        $reg = Pericia::findOrFail($id);

//    $abertura = Abertura::findOrFail($reg->abertura_id);
//    $this->authorize('abertura', $abertura);

        $input = $request->all();

        $reg->fill($input)->save();

        return $reg;
    }

    /*
     * Exclui o registro do BD.
     */

    public function destroy($id)
    {
        $reg = Pericia::findOrFail($id);
//    $abertura = Abertura::findOrFail($reg->abertura_id);
//    $this->authorize('abertura', $abertura);
        $reg->delete();
    }

}
