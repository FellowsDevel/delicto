<?php

namespace Delicto\Http\Controllers;

use Delicto\Abertura;
use Delicto\Objeto;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Log;

class ObjetoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Retorna a lista como um objeto JSON
     *
     * @return Response
     */
    public function index($id)
    {

        $abertura = Abertura::findOrFail($id);
        $this->authorize('abertura', $abertura);

        $regs = Objeto::where('abertura_id', $id)->get();
        return $regs;
    }

    /**
     * Guarda o registro no BD.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'descricao' => 'required'
        ]);

        $reg = new Objeto;
        $input = $request->all();
        $reg->fill($input)->save();

        return $reg;
    }

    /**
     * Exibe o registro.
     * @param type $id
     * @return type
     */
    public function show($id)
    {

        $reg = Objeto::where('id', $id)->get();
        $r = json_decode($reg);

        $abertura = Abertura::findOrFail($r[0]->abertura_id);

        $this->authorize('abertura', $abertura);

        return $reg;
    }

    /**
     * Atualiza o registro no BD.
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'descricao' => 'required'
        ]);

        $reg = Objeto::findOrFail($id);
        $abertura = Abertura::findOrFail($reg->abertura_id);
        $this->authorize('abertura', $abertura);

        $input = $request->all();

        $reg->fill($input)->save();

        return $reg;
    }

    /**
     * Exclui o registro do BD.
     * @param type $id
     * @return type
     */
    public function destroy($id)
    {

        try {
            $reg = Objeto::findOrFail($id);
            $abertura = Abertura::findOrFail($reg->abertura_id);
            $this->authorize('abertura', $abertura);
            $reg->delete();

        } catch (QueryException $e) {
            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Não é possível apagar este Objeto porque existe Restituição ou Perícia relacionado ou uma Avaliação indireta dependendo dele.';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }

    }

}
