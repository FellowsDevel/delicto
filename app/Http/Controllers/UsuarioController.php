<?php

namespace Delicto\Http\Controllers;

use Delicto\Funcao;
use Delicto\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Log;

class UsuarioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        return redirect('/home');
    }

    private function getFuncoes()
    {
        $funcoes = Funcao::lists('descricao', 'id');

        $func[''] = 'Selecione';

        foreach ($funcoes as $key => $value) {
            $func[$key] = $value;
        };
        return $func;
    }

    public function index()
    {

        $usuario = \Auth::user();

        return view('usuario.index')
            ->with('funcoes', $this->getFuncoes())
            ->with('usuario', $usuario);
    }

    public function update(Request $request)
    {
        $usuario = \Auth::user();

        $input = $request->all();

        $usuario->fill($input);
        if (!$request->get('funcao_id')) {
            $usuario->funcao_id = null;
        }
        $usuario->save();

        return view('usuario.index')
            ->with('funcoes', $this->getFuncoes())
            ->with('usuario', $usuario);
    }

    public function redefine(Request $request)
    {
        $this->validate($request, [
            'senha_atual' => 'required',
            'nova_senha' => 'required',
            'nova_senha2' => 'required'
        ]);

        $senha_atual = $request->senha_atual;
        $nova_senha = $request->nova_senha;
        $nova_senha2 = $request->nova_senha2;

        $usuario = \Auth::user();

        $error_code = 0;
        $error_msg = '';

        if (Hash::check($senha_atual, $usuario->password)) {
            if (Hash::check($nova_senha, $usuario->password)) {
                Log::info("A nova senha não pode ser igual a anterior");
                $error_code = 1;
                $error_msg = 'A nova senha não pode ser igual a anterior';
            } else {
                if ($nova_senha != $nova_senha2) {
                    Log::info("As senhas não conferem");
                    $error_code = 2;
                    $error_msg = 'As senhas não conferem';
                } else {
                    $usuario->password = Hash::make($nova_senha);
                    $usuario->save();
                    Log::info("Senhas atualizadas");
                    Auth::logout();
                    return ['success' => 'Senhas atualizadas'];
                }
            }
        } else {
            Log::info("A senha atual está incorreta");
            $error_code = 3;
            $error_msg = 'A senha atual está incorreta';
        }

        return ['error_code' => $error_code, 'error_msg' => $error_msg];

    }

}


