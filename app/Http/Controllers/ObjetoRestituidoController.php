<?php

namespace Delicto\Http\Controllers;

use Delicto\Abertura;
use Delicto\Objeto;
use Delicto\ObjetoRestituido;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Log;

class ObjetoRestituidoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Retorna a lista como um objeto JSON
     *
     * @return Response
     */
    public function index($id)
    {

        $abertura = Abertura::findOrFail($id);
        $this->authorize('abertura', $abertura);

        $regs = ObjetoRestituido::with('objeto', 'qualificacao.pessoa')
            ->where('abertura_id', $id)->get();
        return $regs;
    }

    /**
     * Retorna a lista como um objeto JSON
     *
     * @param $abertura_id
     * @return mixed
     */
    public function restante($abertura_id)
    {

        $abertura = Abertura::findOrFail($abertura_id);
        $this->authorize('abertura', $abertura);

        $regs = Objeto::whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('objetos_restituidos')
                ->whereRaw('objetos.id = objetos_restituidos.objeto_id and objetos.abertura_id = objetos_restituidos.abertura_id');
        })
            ->where('abertura_id', $abertura_id)->get();

        return $regs;
    }

    /**
     * Retorna lista de Qualificações onde o tipo de pessoa é uma vítima ou recebedor como um objeto JSON
     * @param $abertura_id
     * @return mixed
     */
    public function listaVitimas($abertura_id)
    {
        $abertura = Abertura::findOrFail($abertura_id);
        $this->authorize('abertura', $abertura);

        $regs = \Delicto\Qualificacao::with('pessoa')
            ->where(['abertura_id' => $abertura_id])
            ->whereIn('tipo_pessoa_id', array(4, 6))
            ->get();

        return $regs;
    }


    /**
     * Guarda o registro no BD.
     *
     * @param Request $request
     * @return ObjetoRestituido
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'abertura_id' => 'required',
            'objeto_id' => 'required',
            'qualificacao_id' => 'required'
        ]);

        $reg = new ObjetoRestituido;
        $input = $request->all();
        $reg->fill($input)->save();

        return $reg;
    }


    /**
     * Exibe o registro.
     *
     * @param type $id
     * @return type
     */
    public function show($id)
    {

        $reg = ObjetoRestituido::with('objeto', 'qualificacao.pessoa')
            ->where('id', $id)->get();

        $r = json_decode($reg);
        $abertura = Abertura::findOrFail($r[0]->abertura_id);
        $this->authorize('abertura', $abertura);

        return $reg;
    }

    /**
     * Atualiza o registro no BD.
     *
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'abertura_id' => 'required',
            'objeto_id' => 'required',
            'qualificacao_id' => 'required'
        ]);

        $reg = ObjetoRestituido::findOrFail($id);
        $abertura = Abertura::findOrFail($reg->abertura_id);
        $this->authorize('abertura', $abertura);

        $input = $request->all();

        $reg->fill($input)->save();

        return $reg;
    }

    /**
     * Exclui o registro do BD.
     *
     * @param type $id
     * @return type
     */
    public function destroy($id)
    {
        try {
            $reg = ObjetoRestituido::findOrFail($id);
            $abertura = Abertura::findOrFail($reg->abertura_id);
            $this->authorize('abertura', $abertura);
            $reg->delete();
        } catch (QueryException $e) {
            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Não é possível apagar este Objeto Restituído porque existe(m) Objeto(s) relacionado(s)';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }

}
