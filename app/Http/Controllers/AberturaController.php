<?php

namespace Delicto\Http\Controllers;

use Delicto\Abertura;
use Delicto\ApfdRelatorio;
use Delicto\Attachment;
use Delicto\Circunstancia;
use Delicto\Despacho;
use Delicto\Fianca;
use Delicto\Filedata;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Log;

class AberturaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function despacho(Request $request, $id)
    {
        $ab = Abertura::findOrFail($request->id);
        $this->authorize('abertura', $ab);

        $tipi = Despacho::findOrFail($id);
        $input = $request->all();
        $tipi->fill($input)->save();
    }

    public function fianca(Request $request, $id)
    {

        $ab = Abertura::findOrFail($request->id);
        $this->authorize('abertura', $ab);

        $reg = Fianca::findOrFail($id);
        $input = $request->all();
        $reg->fill($input)->save();
    }

    public function circunstancia(Request $request, $id)
    {

        $ab = Abertura::findOrFail($request->id);
        $this->authorize('abertura', $ab);

        $reg = Circunstancia::findOrFail($id);
        $input = $request->all();
        $reg->fill($input)->save();
    }

    public function adicionaAnexo(Request $request, $abertura_id)
    {
        $ab = Abertura::findOrFail($abertura_id);
        $this->authorize('abertura', $ab);

        if (Input::hasFile('file')) {
            $f = Input::file('file');
            for ($x = 0; $x < count($f); $x++) {
                $att = new Attachment;
                $att->abertura_id = $abertura_id;
                $att->name = $f[$x]->getClientOriginalName();
                $att->mime = $f[$x]->getMimeType();
                $att->size = $f[$x]->getSize();
                $att->save();

                self::persistFile($att->id, $f[$x]->getRealPath());
            }
        }

        return array('success');
    }

    private function persistFile($attach_id, $filePath)
    {
        try {
            $fp = fopen($filePath, "rb");
            while (!feof($fp)) {
                // Make the data mysql insert safe
                $binarydata = fread($fp, 65535);
                $data = new Filedata;
                $data->attachment_id = $attach_id;
                $data->file = $binarydata;
                $data->save();
            }
            fclose($fp);
        } catch (QueryException $e) {
        }
    }

    private function writeFile($filePath, $id)
    {
        try {
            $chunks = Filedata::where(['attachment_id' => $id])->orderBy('id')->get();
            $fp = fopen($filePath, "wb");
            foreach ($chunks as $chunk ) {
                fwrite($fp, $chunk->file);
            }
            fclose($fp);
        } catch (QueryException $e) {
        }
    }

    public function listarAnexos($abertura_id)
    {

        $ab = Abertura::findOrFail($abertura_id);
        $this->authorize('abertura', $ab);

        $anexos = Attachment::where(['abertura_id' => $abertura_id])->orderBy('id')->get();

        return $anexos;
    }

    public function getLinkAnexo($id)
    {
        $file = Attachment::find($id);

        $ab = Abertura::findOrFail($file->abertura_id);
        $this->authorize('abertura', $ab);

        /*
         * TODO
         * Antes de pegar o arquivo do banco, verificar se o mesmo está disponível para baixar e se estiver disponibiliza ele
         */
        $randomDir = md5(time() . $file->id . $file->name . str_random());

        // este diretório tem que ser criado no servidor
        mkdir(public_path() . '/download/' . $randomDir);
        $path = public_path() . '/download/' . $randomDir . '/' . html_entity_decode($file->name);
        $download = url('/download') . '/' . $randomDir . '/' . html_entity_decode($file->name);
        self::writeFile($path, $file->id);
        //file_put_contents($path, base64_decode($file->file));

        /*
         * TODO
         * Verificar forma de marcar o arquivo para ser excluído após um período determinado
         * tipo após o arquivo ser disponibilizado para download o mesmo só ficará disponível neste endereço por 24h ou
         * conforme configuração.
         */


        $resp = array('path' => $download, 'name' => $file->name);
        return $resp;

    }

    public function downloadAnexo($id)
    {
        $file = Attachment::find($id);

        $ab = Abertura::findOrFail($file->abertura_id);
        $this->authorize('abertura', $ab);

        /*
         * TODO
         * Antes de pegar o arquivo do banco, verificar se o mesmo está disponível para baixar e se estiver disponibiliza ele
         */
        $randomDir = md5(time() . $file->id . $file->name . str_random());

        // este diretório tem que ser criado no servidor
        mkdir(public_path() . '/download/' . $randomDir);
        $filename = html_entity_decode($file->name);
        $path = public_path() . '/download/' . $randomDir . '/' . $filename;
        //$download = url('/download') . '/' . $randomDir . '/' . $filename;
        //file_put_contents($path, base64_decode($file->file));
        self::writeFile($path, $file->id);

        $headers = array(
            'Content-Type: ' . $file->mime,
        );

        return response()->download($path, $filename, $headers);

    }

    public function excluiAnexo(Request $request, $id)
    {
        try {

            $anx = Attachment::findOrFail($id);

            $ab = Abertura::findOrFail($anx->abertura_id);
            $this->authorize('abertura', $ab);

            $anx->delete();

        } catch (QueryException $e) {

            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Não pode apagar este Anexo porque possui dependências';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }

    }


    public function apfdrelatorio(Request $request, $id)
    {
        $ab = Abertura::findOrFail($request->abertura_id);
        $this->authorize('abertura', $ab);

        $reg = ApfdRelatorio::findOrFail($id);
        $input = $request->all();
        $reg->fill($input)->save();
    }

}
