<?php

namespace Delicto\Http\Controllers;

use Delicto\Grupo;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GrupoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Exibe a lista de Grupos
     *
     * @return Response
     */
    public function index()
    {
        $registros = DB::table('grupos')->paginate(QTD_REGISRTOS_PAGINACAO);
        return view('grupo.index')->with('registros', $registros);
    }

    /**
     * Exibe o formulário para criar novo registro
     *
     * @return Response
     */
    public function create()
    {
        return view('grupo.create');
    }

    /**
     * Guarda o registro no BD.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'descricao' => 'required'
        ]);

        $grupo = new Grupo;
        $grupo->descricao = $request->descricao;
        $grupo->save();

        return redirect()
            ->action('GrupoController@index')
            ->withInput(['descricao' => $request->descricao]);
    }

    /**
     * Exibe o registro.
     *
     * @param  int $id
     * @return Response
     */
    /*
      public function show($id) {
  
      }
     */

    /**
     * Exibe o formulário para edição do registro.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $grupo = Grupo::find($id);
        return view('grupo.edit')->with('grupo', $grupo);
    }

    /**
     * Atualiza o registro no BD.
     *
     * @param  id $id
     * @return Response
     */
    public function update(Request $req, $id)
    {
        $this->validate($req, [
            'descricao' => 'required'
        ]);

        $grupo = Grupo::find($id);
        $anterior = $grupo->descricao;
        $grupo->descricao = $req->descricao;
        $grupo->save();

        return redirect()
            ->action('GrupoController@index')
            ->withInput(['alterado' => $req->descricao,
                'anterior' => $anterior]);
    }

    /**
     * Exclui o registro do BD.
     *
     * @param  Grupo $grupo
     * @return Response
     */
    public function destroy(Grupo $grupo)
    {
        try {
            $grupo->delete();
            return redirect('/grupo');
        } catch (QueryException $e) {

            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Este Grupo não pode ser removido pois o mesmo está sendo utilizado';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }

}
