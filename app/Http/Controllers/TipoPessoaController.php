<?php

namespace Delicto\Http\Controllers;

use Delicto\TipoPessoa;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TipoPessoaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Exibe a lista de Grupos
     *
     * @return Response
     */
    public function index()
    {
        $lista = DB::table('tipo_pessoas')->orderBy('descricao', 'asc')->paginate(QTD_REGISRTOS_PAGINACAO);
        return view('tipo_pessoa.index')->with('registros', $lista);
    }

    /**
     * Exibe o formulário para criar novo registro
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_pessoa.create');
    }

    /**
     * Guarda o registro no BD.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'descricao' => 'required'
        ]);

        $tipo_pessoa = new TipoPessoa;
        $tipo_pessoa->descricao = $request->descricao;
        $tipo_pessoa->save();

        return redirect()
            ->action('TipoPessoaController@index')
            ->withInput(['descricao' => $request->descricao]);
    }

    /**
     * Exibe o registro.
     *
     * @param  int $id
     * @return Response
     */
    /*
      public function show($id) {
  
      }
     */

    /**
     * Exibe o formulário para edição do registro.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $tipo_pessoa = TipoPessoa::find($id);
        return view('tipo_pessoa.edit')->with('tipo_pessoa', $tipo_pessoa);
    }

    /**
     * Atualiza o registro no BD.
     *
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'descricao' => 'required'
        ]);

        $tipo_pessoa = TipoPessoa::find($id);
        $anterior = $tipo_pessoa->descricao;
        $tipo_pessoa->descricao = $request->descricao;
        $tipo_pessoa->save();

        return redirect()
            ->action('TipoPessoaController@index')
            ->withInput(['alterado' => $request->descricao,
                'anterior' => $anterior]);
    }

    /**
     * Exclui o registro do BD.
     *
     * @param  TipoPessoa $tipo_pessoa
     * @return Response
     */
    public function destroy(TipoPessoa $tipo_pessoa)
    {
        try {
            $tipo_pessoa->delete();
            return redirect('/tipo_pessoa');
        } catch (QueryException $e) {

            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'Este Tipo de Pessoa não pode ser removido pois o mesmo está sendo utilizado';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }

}
