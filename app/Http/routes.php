<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('/home');
    }
    return view('auth.login');
});


Route::auth();
Route::get('/home', 'HomeController@index');

//
//
// Utilizando o "resource" o sistema cria as rotas automaticamente como:
//| POST      | grupo                     | grupo.store   | Delicto\Http\Controllers\GrupoController@store                      | web,auth   |
//| GET|HEAD  | grupo                     | grupo.index   | Delicto\Http\Controllers\GrupoController@index                      | web,auth   |
//| GET|HEAD  | grupo/create              | grupo.create  | Delicto\Http\Controllers\GrupoController@create                     | web,auth   |
//| PUT|PATCH | grupo/{grupo}             | grupo.update  | Delicto\Http\Controllers\GrupoController@update                     | web,auth   |
//| DELETE    | grupo/{grupo}             | grupo.destroy | Delicto\Http\Controllers\GrupoController@destroy                    | web,auth   |
//| GET|HEAD  | grupo/{grupo}             | grupo.show    | Delicto\Http\Controllers\GrupoController@show                       | web,auth   |
//| GET|HEAD  | grupo/{grupo}/edit        | grupo.edit    | Delicto\Http\Controllers\GrupoController@edit                       | web,auth   |

Route::resource('usuario', 'UsuarioController');   // Página de exibição da lista
Route::post('/redefinesenha', 'UsuarioController@redefine');

Route::resource('funcao', 'FuncaoController');   // Página de exibição da lista
Route::resource('grupo', 'GrupoController');   // Página de exibição da lista
Route::resource('tipo_pessoa', 'TipoPessoaController');   // Página de exibição da lista
Route::resource('tipo_objeto', 'TipoObjetoController');   // Página de exibição da lista


Route::resource('abertura', 'AberturaController');   // Página de exibição da lista
Route::resource('apfd', 'ApfdController');   // Página de exibição da lista
Route::resource('portaria', 'PortariaController');   // Página de exibição da lista


Route::get('/cidades/{estado}', 'Utils@cidades');
Route::get('/cep/{cep}', 'Utils@cep');


Route::get('/relatorio/apfd/{abertura_id}', 'RelatorioController@rel_apdf');


Route::get('/avaliacao/{abertura_id}', 'Utils@avaliacoes');
Route::get('/avaliacao/restante/{abertura_id}', 'Utils@objetosAvaliacaoRestante');
Route::post('/avaliacao/{id}', 'Utils@atualizaAvaliacao');
Route::post('/avaliacao/{abertura_id}/objeto', 'Utils@adicionaObjetoAvaliacao');
Route::delete('/avaliacao/{abertura_id}/{id}', 'Utils@removeObjetoAvaliacao');


// Controllers relacionados ao APFD e por isso não possuem um controller próprio
Route::post('/despacho/{id}', 'AberturaController@despacho');
Route::post('/fianca/{id}', 'AberturaController@fianca');
Route::post('/apfdrelatorio/{id}', 'AberturaController@apfdrelatorio');
Route::post('/circunstancia/{id}', 'AberturaController@circunstancia');

// Anexos
Route::get('/attachments/{abertura_id}', 'AberturaController@listarAnexos');
Route::get('/attachment/{id}', 'AberturaController@downloadAnexo');
Route::post('/attachment/{abertura_id}', 'AberturaController@adicionaAnexo');
Route::delete('/attachment/{id}', 'AberturaController@excluiAnexo');


// Rotas de Qualificações
// Utiliza apenas JSON
Route::get('/qualificacao/abertura/{abertura_id}', 'QualificacaoController@index');   // Retorna lista de Qualificações de um APFD
Route::post('/qualificacao', 'QualificacaoController@store');   // Inclui registro
Route::get('/qualificacao/{id}', 'QualificacaoController@show');    // Retorna registro
Route::post('/qualificacao/{id}', 'QualificacaoController@update');  // Atualiza o registro
Route::delete('/qualificacao/{id}', 'QualificacaoController@destroy'); // Exclui o registro

// Rotas de Ouvidas
// Utiliza apenas JSON
Route::get('/ouvida/abertura/{id}', 'OuvidaController@index');   // Retorna lista de Ouvidas de um APFD
Route::get('/ouvida/abertas/{abertura_id}', 'OuvidaController@ouvidasAbertas');   // Retorna lista de Ouvidas de um APFD
Route::post('/ouvida', 'OuvidaController@store');   // Inclui registro
Route::get('/ouvida/{id}', 'OuvidaController@show');    // Retorna registro
Route::post('/ouvida/{id}', 'OuvidaController@update');  // Atualiza o registro
Route::delete('/ouvida/{id}', 'OuvidaController@destroy'); // Exclui o registro

// Rotas de Objetos de Perícia
// Utiliza apenas JSON
Route::get('/pericia/abertura/{id}', 'PericiaController@index');   // Retorna lista de Perícias de um APFD
Route::get('/pericia/restante/{abertura_id}', 'PericiaController@restante');   // Retorna lista de Objetos de um APFD que não foram periciados
Route::post('/pericia', 'PericiaController@store');   // Inclui registro
Route::get('/pericia/{id}', 'PericiaController@show');    // Retorna registro
Route::post('/pericia/{id}', 'PericiaController@update');  // Atualiza o registro
Route::delete('/pericia/{id}', 'PericiaController@destroy'); // Exclui o registro

// Rotas de Objeto
// Utiliza apenas JSON
Route::get('/objeto/abertura/{id}', 'ObjetoController@index');   // Retorna lista de Objetos de um APFD
Route::post('/objeto', 'ObjetoController@store');   // Inclui registro
Route::get('/objeto/{id}', 'ObjetoController@show');    // Retorna registro
Route::post('/objeto/{id}', 'ObjetoController@update');  // Atualiza o registro
Route::delete('/objeto/{id}', 'ObjetoController@destroy'); // Exclui o registro

// Rotas de Objeto Restituido
// Utiliza apenas JSON
Route::get('/restituido/abertura/{id}', 'ObjetoRestituidoController@index');   // Retorna lista de Objetos Restituidos de um APFD
Route::get('/restituido/restante/{abertura_id}', 'ObjetoRestituidoController@restante');   // Retorna lista de Objetos de um APFD que não foram restituidos 
Route::get('/restituido/vitimas/{abertura_id}', 'ObjetoRestituidoController@listaVitimas');   // Retorna lista de vitimas de um APFD
Route::post('/restituido', 'ObjetoRestituidoController@store');   // Inclui registro
Route::get('/restituido/{id}', 'ObjetoRestituidoController@show');    // Retorna registro
Route::post('/restituido/{id}', 'ObjetoRestituidoController@update');  // Atualiza o registro
Route::delete('/restituido/{id}', 'ObjetoRestituidoController@destroy'); // Exclui o registro

// Rotas de Boletim Individual
// Utiliza apenas JSON
Route::get('/boletim/abertura/{abertura_id}', 'BoletimIndividualController@index');   // Retorna lista de um APFD
Route::get('/boletim/restante/{abertura_id}', 'BoletimIndividualController@restante');   // Retorna lista de Autuados de um APFD que não foram restituidos
Route::post('/boletim', 'BoletimIndividualController@store');   // Inclui registro
Route::get('/boletim/{id}', 'BoletimIndividualController@show');    // Retorna registro
Route::post('/boletim/{id}', 'BoletimIndividualController@update');  // Atualiza o registro
Route::delete('/boletim/{id}', 'BoletimIndividualController@destroy'); // Exclui o registro

// Rotas da OS
// Utiliza apenas JSON
Route::get('/os/{abertura_id}', 'OSController@index');   // Retorna lista de um APFD
Route::post('/os', 'OSController@store');   // Inclui registro
Route::get('/os/{id}/edit', 'OSController@show');    // Retorna registro
Route::post('/os/{id}/edit', 'OSController@update');  // Atualiza o registro
Route::delete('/os/{id}', 'OSController@destroy'); // Exclui o registro

