<?php

namespace Delicto;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'funcao_id', 'matricula', 'lotacao', 'telefone', 'celular',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function funcao()
    {
        return $this->hasOne(Funcao::class);
    }

    public function verificaPermissao($tela)
    {
        if ($this->id === 2) {
            return true;
        }

        if ($tela === 'tela_configuracao') {
            //return true;
        }
        return false;
    }


}
