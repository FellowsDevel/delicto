<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class Filedata extends Model
{
    protected $fillable = [
        'attachment_id', 'file'
    ];
}
