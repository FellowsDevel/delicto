<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class ObjetoRestituido extends Model
{

    protected $table = 'objetos_restituidos';
    protected $fillable = [
        'abertura_id', 'objeto_id', 'qualificacao_id',
    ];

    /**
     * Um Objeto Restituido pertence a um APDF
     */
    public function abertura()
    {
        return $this->belongsTo(Abertura::class);
    }

    /**
     * Um Objeto Restituido pertence a um objeto
     */
    public function objeto()
    {
        return $this->belongsTo(Objeto::class);
    }

    /**
     * Um Objeto Restituido pertence a uma qualificacao
     */
    public function qualificacao()
    {
        return $this->belongsTo(Qualificacao::class);
    }

}
