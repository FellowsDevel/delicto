<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class TipoPessoa extends Model
{

    /**
     * Um Tipo de Pessoa pode estar em muitas Qualificações
     */
    public function qualificacao()
    {
        return $this->hasMany(Qualificacao::class);
    }

}
