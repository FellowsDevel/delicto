<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class Fianca extends Model
{

    protected $fillable = [
        'abertura_id', 'fianca',
    ];

    /**
     * Uma Fiança pertence a um APDF
     */
    public function abertura()
    {
        return $this->belongsTo(Abertura::class);
    }

}
