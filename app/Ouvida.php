<?php

namespace Delicto;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Log;

class Ouvida extends Model
{

    protected $fillable = [
        'abertura_id', 'qualificacao_id', 'data_ouvida', 'ouvida',
    ];

    /**
     * A qualificação pertence a uma Qualificação
     */
    public function abertura()
    {
        return $this->belongsTo(Abertura::class);
    }

    /**
     * A qualificação pertence a uma Qualificação
     */
    public function qualificacao()
    {
        return $this->belongsTo(Qualificacao::class);
    }

    private function checkData($value)
    {
        return $value !== '-0001-11-30 00:00:00' && $value !== '0000-00-00' && $value !== '';
    }

    public function getDataOuvidaAttribute($value)
    {
        if (self::checkData($value)) {
            return Carbon::parse($value)->format('d/m/Y');
        }
        return '';
    }

    public function setDataOuvidaAttribute($value)
    {
        if (self::checkData($value)) {
            $this->attributes['data_ouvida'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            $this->attributes['data_ouvida'] = 'null';
        }
    }

    protected $dates = ['data_ouvida'];
}
