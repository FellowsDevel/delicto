<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class ObjetoAvaliacao extends Model
{

    protected $table = 'objeto_avaliacoes';
    protected $fillable = [
        'abertura_id', 'avaliacao_indireta_id', 'objeto_id', 'quantidade', 'valor',
    ];

    /**
     * Um Objeto Avaliado pertence a uma avaliação
     */
    public function avaliacao()
    {
        return $this->belongsTo(AvaliacaoIndireta::class);
    }

    /**
     * Um Objeto Avaliado pertence a um objeto
     */
    public function objeto()
    {
        return $this->belongsTo(Objeto::class);
    }

}
