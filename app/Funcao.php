<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class Funcao extends Model
{

    protected $table = 'funcoes';

    protected $fillable = [
        'descricao',
    ];

}
