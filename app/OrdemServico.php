<?php

namespace Delicto;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OrdemServico extends Model
{
    protected $table = 'ordem_servicos';

    protected $fillable = [
        'abertura_id', 'sequencial', 'data_os', 'diligencia',
    ];

    private function checkData($value)
    {
        return $value != '' && $value !== '-0001-11-30 00:00:00' && $value !== '0000-00-00';
    }

    public function getDataOsAttribute($value)
    {
        if (self::checkData($value)) {
            return Carbon::parse($value)->format('d/m/Y');
        }
        return '';
    }

    public function setDataOsAttribute($value)
    {
        if (self::checkData($value)) {
            $this->attributes['data_os'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            $this->attributes['data_os'] = 'null';
        }
    }

    protected $dates = ['data_os'];

}