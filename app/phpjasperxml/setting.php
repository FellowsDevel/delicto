<?php


$server = "localhost";
$db = getenv('DB_DATABASE');
$user = getenv('DB_USERNAME');
$pass = getenv('DB_PASSWORD');

$version = "0.9d";
$pgport = 5432;
$pchartfolder = dirname(__FILE__) . "/class/pchart2";
