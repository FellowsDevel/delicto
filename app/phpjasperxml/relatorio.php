<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once('class/tcpdf/tcpdf.php');
include_once("class/PHPJasperXML.inc.php");
include_once('setting.php');


if (!isset($nome_relatorio)) {
    return;
}

if (!isset($parametros_relatorio)) {
    $parametros_relatorio = array();
}

$PHPJasperXML = new PHPJasperXML();
//$PHPJasperXML->debugsql = true;

$PHPJasperXML->arrayParameter = $parametros_relatorio;

$PHPJasperXML->load_xml_file(dirname(__FILE__) . "/$nome_relatorio.jrxml");

$PHPJasperXML->transferDBtoArray($server, $user, $pass, $db);
$PHPJasperXML->outpage("I");    //page output method I:standard output  D:Download file

?>
