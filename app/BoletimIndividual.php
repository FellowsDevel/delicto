<?php

namespace Delicto;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Log;

class BoletimIndividual extends Model
{

    protected $table = 'boletins_individuais';
    protected $fillable = [
        'abertura_id', 'qualificacao_id', 'distrito', 'zona', 'data', 'periodo',
        'praticado', 'lugar_ocorrencia', 'meio_empregado', 'motivos_presumiveis', 'uf_boletim',
        'infracao_prevista', 'preso', 'data_inquerito'
    ];
    protected $dates = ['data', 'data_inquerito'];

    /**
     * um boletim possui uma qualificacao com tipo de pessoa: Autuado (1)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function qualificacao()
    {
        return $this->belongsTo(Qualificacao::class);
    }

    public function getDataAttribute($value)
    {
        if ($value && self::checkData($value)) {
            return Carbon::parse($value)->format('d/m/Y');
        }
        return '';
    }

    private function checkData($value)
    {
        return $value != 'null' && $value !== '-0001-11-30 00:00:00' && $value !== '0000-00-00' && $value !== '';
    }

    public function setDataAttribute($value)
    {
        if (self::checkData($value)) {
            $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            $this->attributes['data'] = 'null';
        }
    }

    public function getDataInqueritoAttribute($value)
    {
        if ($value && self::checkData($value)) {
            return Carbon::parse($value)->format('d/m/Y');
        }
        return '';
    }

    public function setDataInqueritoAttribute($value)
    {
        if (self::checkData($value)) {
            $this->attributes['data_inquerito'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            $this->attributes['data_inquerito'] = 'null';
        }
    }
}

