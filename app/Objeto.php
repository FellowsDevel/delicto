<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class Objeto extends Model
{

    protected $fillable = [
        'abertura_id', 'descricao',
    ];

    /**
     * Um objeto pertence a um APDF
     */
    public function abertura()
    {
        return $this->belongsTo(Abertura::class);
    }

}
