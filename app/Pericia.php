<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class Pericia extends Model
{

    protected $fillable = [
        'abertura_id', 'tipo_objeto_id', 'objeto_id',
    ];

    /**
     * A Perícia pertence a um APDF
     */
    public function abertura()
    {
        return $this->belongsTo(Abertura::class);
    }

    /**
     * Uma Perícia pertence a um Objeto
     */
    public function objeto()
    {
        return $this->belongsTo(Objeto::class);
    }

    /**
     * Uma Perícia possui um Tipo de Objeto
     */
    public function tipoObjeto()
    {
        return $this->belongsTo(TipoObjeto::class);
    }

}
