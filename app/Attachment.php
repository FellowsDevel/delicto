<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{

    protected $fillable = [
        'abertura_id', 'name', 'mime', 'size'
    ];

}
