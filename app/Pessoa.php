<?php

namespace Delicto;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Log;

class Pessoa extends Model
{

    /**
     * Uma pessoa pode estar em diversas Qualificaçoes
     */
    public function qualificacao()
    {
        return $this->hasOne(Qualificacao::class);
    }

    protected $fillable = [
        'apelido', 'rg', 'cpf', 'ocupacao', 'matricula', 'local_trabalho',
        'estado_civil', 'nacionalidade', 'naturalidade', 'data_nascimento',
        'idade', 'nome_mae', 'nome_pai', 'escolaridade', 'sexo', 'cep', 'logradouro',
        'numero', 'complemento', 'referencia', 'bairro', 'cidade', 'uf',
        'telefone_1', 'telefone_2', 'advogado'
    ];

    private function checkData($value)
    {
        return $value !== '-0001-11-30 00:00:00' && $value !== '0000-00-00' && $value !== '';
    }

    public function getDataNascimentoAttribute($value)
    {
        if (self::checkData($value)) {
            return Carbon::parse($value)->format('d/m/Y');
        }
        return '';
    }

    public function setDataNascimentoAttribute($value)
    {
        if (self::checkData($value)) {
            $this->attributes['data_nascimento'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            $this->attributes['data_nascimento'] = 'null';
        }
    }

    protected $dates = ['data_nascimento'];

}
