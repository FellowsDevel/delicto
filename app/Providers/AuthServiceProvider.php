<?php

namespace Delicto\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Log;

class AuthServiceProvider extends ServiceProvider {
  
  /**
   * The policy mappings for the application.
   *
   * @var array
   */
  protected $policies = [
    'Delicto\Model' => 'Delicto\Policies\ModelPolicy',
  ];
  
  /**
   * Register any application authentication / authorization services.
   *
   * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
   * @return void
   */
  public function boot( GateContract $gate ) {
    $this->registerPolicies( $gate );
    
    $gate->define( 'abertura', function ( $user, $abertura ) {
      return $user->id == $abertura->user_id;
    } );
    
    $gate->before( function ( $user, $ability ) {
      if ( $user->id == 2 && $user->name == 'Administrador' ) {
        return true;
      }
    } );
  }
  
}
