<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class ApfdRelatorio extends Model
{

    protected $fillable = [
        'abertura_id', 'relatorio',
    ];

    /**
     * Uma Fiança pertence a um APDF
     */
    public function abertura()
    {
        return $this->belongsTo(Abertura::class);
    }
}
