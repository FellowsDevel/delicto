<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class Despacho extends Model
{

    protected $fillable = [
        'abertura_id', 'despacho',
    ];

    public function abertura()
    {
        return $this->belongsTo(Abertura::class);
    }

}
