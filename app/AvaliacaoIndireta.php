<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class AvaliacaoIndireta extends Model
{

    protected $table = 'avaliacoes_indiretas';
    protected $fillable = [
        'abertura_id', 'perito_1_nome', 'perito_1_matricula', 'perito_2_nome', 'perito_2_matricula',
    ];

    /**
     * Um Abertura possui diversas Qualificaçoes
     */
    public function objetos()
    {
        return $this->hasMany(ObjetoAvaliacao::class);
    }

}
