<?php

namespace Delicto;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Log;

class Abertura extends Model
{

    /**
     * Um Abertura possui diversas Qualificaçoes
     */
    public function qualificacoes()
    {
        return $this->hasMany(Qualificacao::class);
    }

    /**
     * Um Abertura possui diversas Qualificaçoes
     */
    public function ouvidas()
    {
        return $this->hasMany(Ouvida::class);
    }

    /**
     * Um Abertura possui diversas Pericias
     */
    public function pericias()
    {
        return $this->hasMany(Pericia::class);
    }

    /**
     * Um Abertura possui um Despacho
     */
    public function despacho()
    {
        return $this->hasOne(Despacho::class);
    }

    /**
     * Um Abertura possui uma Fiança
     */
    public function fianca()
    {
        return $this->hasOne(Fianca::class);
    }

    /**
     * Um Abertura possui uma Avaliação
     */
    public function avaliacao()
    {
        return $this->hasOne(AvaliacaoIndireta::class);
    }

    /**
     * Um Abertura possui um ApfdRelatorio
     */
    public function apfdrelatorio()
    {
        return $this->hasOne(ApfdRelatorio::class);
    }
    
    /**
     * Um Abertura possui um ApfdRelatorio
     */
    public function circunstancia()
    {
        return $this->hasOne(Circunstancia::class);
    }


    protected $fillable = [
        'delegado', 'abertura_cidade', 'abertura_estado', 'hora', 'data', 'bo', 'circunscricao', 'tombamento'
    ];

    private function checkData($value)
    {
        return $value !== '-0001-11-30 00:00:00' && $value !== '0000-00-00' && $value !== '';
    }

    public function getDataAttribute($value)
    {
        if (self::checkData($value)) {
            return Carbon::parse($value)->format('d/m/Y');
        }
        return '';
    }

    public function setDataAttribute($value)
    {
        if (self::checkData($value)) {
            $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            $this->attributes['data'] = 'null';
        }
    }

    protected $dates = ['data'];

}
