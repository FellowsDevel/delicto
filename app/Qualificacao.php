<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class Qualificacao extends Model
{

    protected $table = 'qualificacoes';

    protected $fillable = [
        'abertura_id', 'tipo_pessoa_id', 'pessoa_id',
    ];

    /**
     * Uma Qualificação possui um tipo de pessoa associada
     */
    public function tipoPessoa()
    {
        return $this->belongsTo(TipoPessoa::class);
    }

    /**
     * Uma Qualificação pertence a um APDF
     */
    public function abertura()
    {
        return $this->belongsTo(Abertura::class);
    }

    /**
     * Uma Qualificação possui uma Pessoa mas esta Pessoa pode estar em
     * mais de uma Qualificação.
     * Então precisamos de uma cardinalidade de muitos-para-muitos.
     */
    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class);
    }

    /**
     * Uma Qualificação possui uma Ouvida
     */
    public function ouvida()
    {
        return $this->hasOne(Ouvida::class);
    }

}
