<?php

namespace Delicto;

use Illuminate\Database\Eloquent\Model;

class Circunstancia extends Model
{
    protected $fillable = [
        'abertura_id', 'circunstancia',
    ];

    public function abertura()
    {
        return $this->belongsTo(Abertura::class);
    }
}
