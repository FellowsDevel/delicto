<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GruposSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupos')->insert([
            ['descricao' => 'Grupo 1'],
            ['descricao' => 'Grupo 2'],
            ['descricao' => 'Grupo 3'],
            ['descricao' => 'Grupo 4']
        ]);
    }

}
