<?php

use Illuminate\Database\Seeder;

class UsuarioSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            ['name' => 'André Fellows', 'email' => 'andrefellows@bitsolucoes.com.br', 'password' => Hash::make('fellows')]);
        DB::table('users')->insert(
            ['name' => 'Administrador', 'email' => 'admin@bitsolucoes.com.br', 'password' => Hash::make('bitsolucoes@3')]);
    }

}
