<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FuncaoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('funcoes')->insert([
            ['descricao' => 'Agente de Polícia'],
            ['descricao' => 'Delegado de Polícia'],
            ['descricao' => 'Escrivão de Polícia']
        ]);
    }
}
