<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoObjetoSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_objetos')->insert([
            ['descricao' => 'Arma'],
            ['descricao' => 'Celular'],
            ['descricao' => 'Droga'],
        ]);
    }

}
