<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPessoaSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_pessoas')->insert([
            ['descricao' => 'Autuado'],
            ['descricao' => 'Condutor'],
            ['descricao' => 'Contato Família'],
            ['descricao' => 'Recebedor'],
            ['descricao' => 'Testemunha'],
            ['descricao' => 'Vítima'],
        ]);
    }

}
