<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UsuarioSeed::class);
        $this->call(GruposSeed::class);
        $this->call(TipoPessoaSeed::class);
        $this->call(TipoObjetoSeed::class);
        $this->call(FuncaoSeed::class);
    }
}
