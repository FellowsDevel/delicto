<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObjetoAvaliacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objeto_avaliacoes', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('abertura_id')->length(10)->unsigned()->nullable(false);
            $table->integer('avaliacao_indireta_id')->length(10)->unsigned()->nullable(false);
            $table->integer('objeto_id')->length(10)->unsigned()->nullable(false);
            $table->integer('quantidade')->length(6)->nullable();
            $table->decimal('valor', 10, 2)->nullable();

            $table->unique(array('abertura_id', 'avaliacao_indireta_id', 'objeto_id'), 'obj_avaliacao_UK');
            $table->index(array('abertura_id', 'avaliacao_indireta_id', 'objeto_id'), 'obj_avaliacao_IDX');

            $table->timestamps();

            $table->foreign('abertura_id')->references('id')->on('aberturas');
            $table->foreign('avaliacao_indireta_id')->references('id')->on('avaliacoes_indiretas');
            $table->foreign('objeto_id')->references('id')->on('objetos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('objeto_avaliacoes');
    }
}
