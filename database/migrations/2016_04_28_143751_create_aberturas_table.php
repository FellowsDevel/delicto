<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAberturasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aberturas', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            // Estrutura da tabela 
            $table->increments('id');

            // Para que seja possível criar FKs é necessário que os campos que 
            // referenciarao os campo da outra tabela possuam o mesmo tipo do campo 
            // da tabela da informação "pai".
            // Como utilizamos o "increments" como PK, seu tipo é
            //   UNSIGNED INTEGER de 10 digitos
            // Então temos que criar o campo explicitando se não ocorre erro!!
            $table->integer('grupo_id')->length(10)->unsigned()->nullable();
            $table->integer('user_id')->length(10)->unsigned()->nullable(false);
            $table->enum('tipo_registro', ['APFD', 'TCO', 'PORTARIA', 'AAFAI', 'BOC', 'PEM_AIAI'])->nullable(false);

            $table->string('delegado', 120)->nullable(false);
            $table->string('circunscricao', 120)->nullable(false);

            $table->string('abertura_cidade', 120)->nullable();
            $table->string('abertura_estado', 2)->nullable();
            $table->string('hora', 5)->nullable();
            $table->date('data')->nullable();
            $table->text('bo')->nullable();
            $table->string('tombamento', 60)->nullable();

            $table->longText('despacho')->nullable();
            $table->longText('fianca')->nullable();

            $table->timestamps();

            // Índices
            $table->index('grupo_id');
            $table->index('user_id');

            // Chaves extrangeiras 
            $table->foreign('grupo_id')->references('id')->on('grupos');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aberturas');
    }

}
