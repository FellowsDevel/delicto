<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAvaliacaoIndiretasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacoes_indiretas', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('abertura_id')->length(10)->unsigned()->nullable(false);
            $table->string('perito_1_nome', 120)->nullable();
            $table->string('perito_1_matricula', 120)->nullable();
            $table->string('perito_2_nome', 120)->nullable();
            $table->string('perito_2_matricula', 120)->nullable();

            $table->timestamps();

            $table->foreign('abertura_id')->references('id')->on('aberturas')->onDelete('Cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('avaliacoes_indiretas');
    }
}
