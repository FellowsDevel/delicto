<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePessoasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('nome', 120)->index()->nullable(false);
            $table->string('apelido', 120)->nullable();
            $table->string('rg', 200)->nullable();
            $table->string('cpf', 20)->nullable();
            $table->string('ocupacao', 150)->nullable();
            $table->string('matricula', 50)->nullable();
            $table->string('local_trabalho', 250)->nullable();
            $table->enum('estado_civil', ['Solteiro(a)',
                'Casado(a)',
                'Amasiado(a)',
                'União Estável',
                'Separado(a)',
                'Divorciado(a)',
                'Viúvo(a)'])->nullable();
            $table->string('nacionalidade', 80)->nullable();
            $table->string('naturalidade', 80)->nullable();
            $table->date('data_nascimento')->nullable();
            $table->string('idade', 3)->nullable();
            $table->string('nome_mae', 120)->nullable();
            $table->string('nome_pai', 120)->nullable();
            $table->enum('escolaridade', ['Fundamental - Incompleto',
                'Fundamental - Completo',
                'Médio - Incompleto',
                'Médio - Completo',
                'Superior - Incompleto',
                'Superior - Completo',
                'Pós-graduação - Incompleto',
                'Pós-graduação - Completo',
                'Mestrado - Incompleto',
                'Mestrado - Completo',
                'Doutorado - Incompleto',
                'Doutorado - Completo'])->nullable();

            $table->enum('sexo', ['Feminino', 'Masculino'])->nullable();
            $table->string('responsavel', 120)->nullable();
            $table->string('cep', 10)->nullable();
            $table->string('logradouro', 180)->nullable();
            $table->string('numero', 10)->nullable();
            $table->string('complemento', 100)->nullable();
            $table->string('referencia', 150)->nullable();
            $table->string('bairro', 60)->nullable();
            $table->string('cidade', 80)->nullable();
            $table->string('uf', 2)->nullable();
            $table->string('telefone_1', 25)->nullable();
            $table->string('telefone_2', 25)->nullable();
            $table->text('advogado')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pessoas');
    }

}
