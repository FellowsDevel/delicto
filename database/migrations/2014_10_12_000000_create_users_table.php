<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');

            $table->integer('funcao_id')->length(10)->unsigned()->nullable();
            $table->string('matricula', 120)->nullable();
            $table->string('lotacao', 120)->nullable();
            $table->string('telefone', 25)->nullable();
            $table->string('celular', 25)->nullable();

            $table->rememberToken();
            $table->timestamps();

            $table->foreign('funcao_id')->references('id')->on('funcoes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

}
