<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePericiasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pericias', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('abertura_id')->length(10)->unsigned()->nullable(false);
            $table->integer('objeto_id')->length(10)->unsigned()->nullable(false);
            $table->integer('tipo_objeto_id')->length(10)->unsigned()->nullable(false);
            $table->timestamps();

            // Índices
            $table->index('abertura_id');
            $table->index('objeto_id');
            $table->index('tipo_objeto_id');

            // Chaves extrangeiras 
            $table->foreign('abertura_id')->references('id')->on('aberturas');
            $table->foreign('objeto_id')->references('id')->on('objetos');
            $table->foreign('tipo_objeto_id')->references('id')->on('tipo_objetos');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pericias');
    }

}
