<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObjetosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objetos', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('abertura_id')->length(10)->unsigned()->nullable(false);
            $table->string('descricao', 120)->nullable(false)->index();
            $table->timestamps();

            $table->unique(array('abertura_id', 'descricao'));

            $table->index('abertura_id');
            $table->foreign('abertura_id')->references('id')->on('aberturas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('objetos');
    }

}
