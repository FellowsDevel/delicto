<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObjetosRestituidosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objetos_restituidos', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('abertura_id')->length(10)->unsigned()->nullable(false);
            $table->integer('objeto_id')->length(10)->unsigned()->nullable(false);
            $table->integer('qualificacao_id')->length(10)->unsigned()->nullable(false);
            $table->timestamps();

            $table->unique(array('abertura_id', 'objeto_id', 'qualificacao_id'));
            $table->index(array('abertura_id', 'objeto_id', 'qualificacao_id'));

            $table->foreign('abertura_id')->references('id')->on('aberturas');
            $table->foreign('objeto_id')->references('id')->on('objetos');
            $table->foreign('qualificacao_id')->references('id')->on('qualificacoes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('objetos_restituidos');
    }

}
