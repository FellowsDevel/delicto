<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOuvidasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ouvidas', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('abertura_id')->length(10)->unsigned()->nullable(false);
            $table->integer('qualificacao_id')->length(10)->unsigned()->nullable(false);
            $table->date('data_ouvida')->nullable();
            $table->longText('ouvida')->nullable();
            $table->timestamps();

            // Índices
            $table->index('abertura_id');
            $table->index('qualificacao_id');

            // Chaves extrangeiras 
            $table->foreign('abertura_id')->references('id')->on('aberturas');
            $table->foreign('qualificacao_id')->references('id')->on('qualificacoes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ouvidas');
    }

}
