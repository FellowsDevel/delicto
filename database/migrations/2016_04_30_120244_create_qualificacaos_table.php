<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQualificacaosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qualificacoes', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('abertura_id')->length(10)->unsigned()->index()->nullable(false);
            $table->foreign('abertura_id')->references('id')->on('aberturas');

            $table->integer('tipo_pessoa_id')->length(10)->unsigned()->index()->nullable(false);
            $table->foreign('tipo_pessoa_id')->references('id')->on('tipo_pessoas');

            $table->integer('pessoa_id')->length(10)->unsigned()->index()->nullable(false);
            $table->foreign('pessoa_id')->references('id')->on('pessoas');

            $table->longText('tipificacao')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('qualificacoes');
    }

}
