<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBoletimIndividualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletins_individuais', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('abertura_id')->length(10)->unsigned()->nullable(false);
            $table->integer('qualificacao_id')->length(10)->unsigned()->nullable(false);
            $table->string('uf_boletim', 2)->nullable();
            $table->string('distrito', 120)->nullable();
            $table->enum('zona', ['Urbana', 'Rural'])->nullable();
            $table->date('data')->nullable();
            $table->enum('periodo', ['Dia', 'Noite'])->nullable();
            $table->enum('praticado', ['Trabalho', 'Domingo', 'Feriado', 'Dia Santificado'])->nullable();
            $table->text('lugar_ocorrencia')->nullable();
            $table->text('meio_empregado')->nullable();
            $table->text('motivos_presumiveis')->nullable();
            $table->text('infracao_prevista')->nullable();
            $table->enum('preso', ['S', 'N'])->nullable();
            $table->date('data_inquerito')->nullable();

            $table->timestamps();

            $table->foreign('abertura_id')->references('id')->on('aberturas');
            $table->foreign('qualificacao_id')->references('id')->on('qualificacoes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('boletins_individuais');
    }
}
