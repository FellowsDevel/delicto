<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('abertura_id')->length(10)->unsigned()->nullable(false);
            $table->string('name', 255)->nullable(false);
            $table->string('mime', 255)->nullable();
            $table->integer('size')->length(10)->unsigned()->nullable(false);

            $table->timestamps();
            $table->foreign('abertura_id')->references('id')->on('aberturas')->onDelete('Cascade');

            // Para criar um longblob no eloquent é necessário alterar os arquivos:
            // vendor\laravel\framework\src\Illuminate\Database\Schema\Grammars\MySqlGrammar.php
            //  e
            // vendor\laravel\framework\src\Illuminate\Database\Schema\Grammars\MySqlGrammar.php
            //
            // Deve procurar por BINARY e criar métodos parecidos para mediumblob e longblob
            //
            //$table->longblob( 'file')->nullable(false);


        });

        // para utilizar o eloquent do jeito que está, tem que adicionar o longblob por fora
        //DB::statement("ALTER TABLE `attachments` ADD COLUMN `file` LONGBLOB NOT NULL AFTER `size`");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attachments');
    }
}
