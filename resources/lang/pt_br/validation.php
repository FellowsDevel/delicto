<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O campo :attribute deve ser confirmado.',
    'active_url'           => 'O campo :attribute não é uma URL válida.',
    'after'                => 'O campo :attribute deve ser uma data posterior a data :date.',
    'alpha'                => 'O campo :attribute deve conter apenas letras.',
    'alpha_dash'           => 'O campo :attribute deve conter apenas letras, números e hífen.',
    'alpha_num'            => 'O campo :attribute deve conter apenas letras e números.',
    'array'                => 'O campo :attribute deve ser um array.',
    'before'               => 'O campo :attribute deve ser uma data anterior a data :date.',
    'between'              => [
        'numeric' => 'O campo :attribute deve estar entre :min e :max.',
        'file'    => 'O campo :attribute deve ter entre :min e :max kilobytes.',
        'string'  => 'O campo :attribute deve conter entre :min e :max caracteres.',
        'array'   => 'O campo :attribute deve possuir entre :min e :max itens.',
    ],
    'boolean'              => 'O campo :attribute deve ser true ou false.',
    'confirmed'            => 'O campo :attribute de confirmação não coincide.',
    'date'                 => 'O campo :attribute não é uma data válida.',
    'date_format'          => 'O campo :attribute não coincide com o formato :format.',
    'different'            => 'O campo :attribute e :other devem ser diferentes.',
    'digits'               => 'O campo :attribute deve possuir :digits dígitos.',
    'digits_between'       => 'O campo :attribute deve possuir entre :min e :max dígitos.',
    'distinct'             => 'O campo :attribute possui valor duplicado.',
    'email'                => 'O campo :attribute deve ser um email válido.',
    'exists'               => 'O campo selecionado :attribute é inválido.',
    'filled'               => 'O campo :attribute é obrigatório.',
    'image'                => 'O campo :attribute deve ser uma imagem.',
    'in'                   => 'O campo selecionado :attribute é inválido.',
    'in_array'             => 'O valor do campo :attribute não existe em :other.',
    'integer'              => 'O valor do campo :attribute deve ser do tipo inteiro.',
    'ip'                   => 'O campo :attribute deve ser um endereço IP válido.',
    'json'                 => 'O campo :attribute deve ser uma strin JSON válida.',
    'max'                  => [
        'numeric' => 'O campo :attribute não pode ser maior que :max.',
        'file'    => 'O campo :attribute não pode ser maior que :max kilobytes.',
        'string'  => 'O campo :attribute não pode conter mais que :max caracteres.',
        'array'   => 'O campo :attribute não pode conter mais de :max itens.',
    ],
    'mimes'                => 'O campo :attribute deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => 'O campo :attribute deve ser no mínimo :min.',
        'file'    => 'O campo :attribute deve ter ao menos :min kilobytes.',
        'string'  => 'O campo  :attribute deve possuir ao menos :min caracteres.',
        'array'   => 'O campo :attribute deve possuir ao menos :min itens.',
    ],
    'not_in'               => 'O campo seleciondo :attribute é inválido.',
    'numeric'              => 'O campo :attribute deve ser um número.',
    'present'              => 'O campo :attribute deve estar presente.',
    'regex'                => 'O campo :attribute está num formato inválido.',
    'required'             => 'O campo :attribute é obrigatório.',
    'required_if'          => 'O campo :attribute é obrigatório que o campo :other é :value.',
    'required_unless'      => 'O campo :attribute é obrigatório a menos que o campo :other seja :values.',
    'required_with'        => 'O campo :attribute é obrigatório quando o valor :values está presente.',
    'required_with_all'    => 'O campo :attribute é obrigatório quando os valores :values estão presentes.',
    'required_without'     => 'O campo :attribute é obrigatório quando o valor :values não está presente.',
    'required_without_all' => 'O campo :attribute é obrigatório quando os valores :values não estão presentes.',
    'same'                 => 'O campo :attribute e :other devem coincidir.',
    'size'                 => [
        'numeric' => 'O campo :attribute deve ser do tamanho :size.',
        'file'    => 'O campo :attribute deve possuir :size kilobytes.',
        'string'  => 'O campo :attribute deve ter :size caracteres.',
        'array'   => 'O campo :attribute deve conter :size itens.',
    ],
    'string'               => 'O campo :attribute deve ser uma String.',
    'timezone'             => 'O campo :attribute deve ser uma zona válida.',
    'unique'               => 'O campo :attribute já foi obtido.',
    'url'                  => 'O formato do campo :attribute é inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
