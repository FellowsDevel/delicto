<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Senhas devem possuir ao menos seis caracteres e coincidir com a senha de confirmação.',
    'reset' => 'Sua senha foi resetada!',
    'sent' => 'Enviamos um email com um link para você resetar sua senha!',
    'token' => 'Este token de reset de senha é inválido.',
    'user' => "Não encontramos um usuário com este endereço de email.",

];
