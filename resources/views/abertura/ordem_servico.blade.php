<div>
    <div class="form-group text-right">
        <button id="nova_os" type="button" class="btn btn-primary btn-sm" data-toggle="modal">
            Nova Ordem de Serviço
        </button>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Sequencial</th>
                <th>Data</th>
                <th>Diligência</th>
                <th class="text-center">Operações</th>
            </tr>
            </thead>
            <tbody id="lista_os">
            </tbody>
        </table>
    </div>


    <div id="confirm_os" class="modal fade" role="dialog" aria-labelledby="confirmDeleteOS" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <span id="confirmDeleteOS">Tem certeza que quer excluir esta Ordem de Serviço?</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete_os">excluir</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


    <div id="formulario_os" class="modal fade" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalLabel">Ordem de Serviço</h4>
                </div>

                <div class="modal-body">
                    <div id="form_os">
                        <fieldset>
                            <input type="hidden" id="os_id" name="os_id" value="">

                            <div class="form-group col-sm-3">
                                {{ Form::label('sequencial', 'Sequencial' ) }}
                                {{ Form::text('sequencial', null , [ 'class' => 'form-control']) }}
                            </div>

                            <div class="form-group col-sm-2">
                                {{ Form::label('data_os', 'Data da OS' ) }}
                                {{ Form::text('data_os', null, ['class' => 'form-control mask_date']) }}
                            </div>

                            <div class="form-group col-sm-12">
                                {{ Form::label('diligencia', 'Diligência' ) }}
                                {{ Form::textarea('diligencia', null, ['rows' => '8', 'class' => 'form-control']) }}
                            </div>
                        </fieldset>

                        <div class="form-group" style="margin-top: 20px; text-align: center">
                            <div>
                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-sm" id="cria_os">
                                    Criar
                                </button>
                                <button type="button" data-dismiss="modal" class="btn btn-default">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">

                </div>

            </div>
        </div>
    </div>
</div>

