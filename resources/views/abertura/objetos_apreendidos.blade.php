<div>
    <div class="table-responsive">
        <div class="form-group">

            <div class="form-group col-sm-4">

                <label for="idDescricaoObjeto">Descrição do objeto</label>
                <textarea
                        id="idDescricaoObjeto"
                        name="descricao_objeto"
                        rows="4"
                        title="Digite a descrição do objeto e pressione Enter ou o botão abaixo"
                        class="form-control"
                        placeholder="Digite a descrição do objeto e pressione Enter ou o botão abaixo"></textarea>

                <button style="margin-top: 10px;" id="novo_objeto" type="button" class="btn btn-primary btn-sm"
                        data-toggle="modal">
                    Adiciona Objeto
                </button>
            </div>

            <div class="form-group col-sm-8">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Relação dos Objetos</th>
                        <th class="text-center">Operações</th>
                    </tr>
                    </thead>
                    <tbody id="lista_objetos">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

