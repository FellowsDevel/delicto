<div>
    <div class="table-responsive">
        <div class="form-group">

            <div class="form-group col-sm-4">
                <div class="form-group">
                    <label for="lista_vitimas">Lista de Pessoas</label>
                    <select id="lista_vitimas" class="form-control" name="vitima"></select>
                </div>
                <div class="form-group">
                    <label for="lista_objetos_a_restituir">Lista de Objetos</label>
                    <select id="lista_objetos_a_restituir" class="form-control" multiple name="objeto[]"
                            size="5"></select>
                </div>
                <button style="margin-top: 10px;" id="associar_objetos" type="button" class="btn btn-primary btn-sm">
                    Associar Objetos
                </button>
            </div>

            <div class="form-group col-sm-8">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Recebedor</th>
                        <th>Descrição do Objeto</th>
                        <th class="text-center">Operações</th>
                    </tr>
                    </thead>
                    <tbody id="lista_objetos_restituidos">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

