<div xmlns="http://www.w3.org/1999/html">
    <div class="form-group text-right">
        <button id="novo_boletim_individual" type="button" class="btn btn-primary btn-sm" data-toggle="modal">
            Novo Boletim
        </button>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Data provável</th>
                <th>Período</th>
                <th>Dia praticado</th>
                <th class="text-center">Operações</th>
            </tr>
            </thead>
            <tbody id="lista_boletim_individual">
            </tbody>
        </table>
    </div>

    <div id="confirm_boletim_individual" class="modal fade" role="dialog"
         aria-labelledby="confirmDeleteBoletimIndividual"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <span id="confirmDeleteBoletimIndividual">Tem certeza que quer excluir este Boletim?</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete_boletim_individual">
                        excluir
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


    <div id="formulario_boletim_individual" class="modal fade" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalLabel">Boletim Individual</h4>
                </div>

                <div class="modal-body">
                    <div id="form_boletim_individual">
                        <fieldset>
                            <input type="hidden" id="id" name="id" value="">

                            <div class="form-group col-sm-9">
                                <label for="boletim_qualificacao_id">Lista de Autuados</label>
                                <select id="boletim_qualificacao_id" class="form-control"
                                        name="boletim_qualificacao_id"></select>
                            </div>

                            <div class="form-group col-sm-3">
                                <label for="data_inquerito">Data do inquérito:</label>
                                <input id="data_inquerito" name="data_inquerito" maxlength="10" type="text"
                                       class="form-control mask_date">
                            </div>


                            <div class="form-group col-sm-4">
                                <label for="uf_boletim">UF do delito:</label>
                                {{ Form::select('uf_boletim', $estados, null, ['id'=> 'uf_boletim', 'class' => 'form-control'] ) }}
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="distrito">Distrito onde ocorreu o delito:</label>
                                {{ Form::select('distrito', $cidades, null, ['id'=> 'distrito', 'class' => 'form-control'] ) }}
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="zona">Ocorreu na zona urbana ou rural:</label>
                                <select id="zona" name="zona" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="Urbana">Urbana</option>
                                    <option value="Rural">Rural</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="data_boletim">Data certa ou provável:</label>
                                <input id="data_boletim" name="data_boletim" maxlength="10" type="text"
                                       class="form-control mask_date">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="periodo">Ocorreu de dia ou à noite:</label>
                                <select id="periodo" name="periodo" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="Dia">Dia</option>
                                    <option value="Noite">Noite</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="praticado">Foi praticado em dia:</label>
                                <select id="praticado" name="praticado" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="Trabalho">Trabalho</option>
                                    <option value="Domingo">Domingo</option>
                                    <option value="Feriado">Feriado</option>
                                    <option value="Dia Santificado">Dia Santificado de Festa</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="lugar_ocorrencia">Lugar da Ocorrência:</label>
                                <textarea id="lugar_ocorrencia" name="lugar_ocorrencia" rows="3"
                                          class="form-control"></textarea>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="meio_empregado">Meio Empregado:</label>
                                <textarea id="meio_empregado" name="meio_empregado" rows="3"
                                          class="form-control"></textarea>
                            </div>
                            <div class="form-group col-sm-5">
                                <label for="motivos_presumiveis">Motivos Presumíveis:</label>
                                <textarea id="motivos_presumiveis" name="motivos_presumiveis" rows="3"
                                          class="form-control"></textarea>
                            </div>

                            <div class="form-group col-sm-5">
                                <label for="infracao_prevista">Infração prevista:</label>
                                <textarea id="infracao_prevista" name="infracao_prevista" rows="3"
                                          class="form-control"></textarea>
                            </div>

                            <div class="form-group col-sm-2">
                                <label for="preso">Preso:</label>
                                <select id="preso" name="preso" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="S">Sim</option>
                                    <option value="N">Não</option>
                                </select>
                            </div>


                        </fieldset>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="form-group" style="margin-top: 20px; text-align: center">
                        <div>
                            <div>
                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
                                        id="envia_boletim_individual">
                                    Criar
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

