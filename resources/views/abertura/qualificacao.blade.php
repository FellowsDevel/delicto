<div xmlns="http://www.w3.org/1999/html">
    <div class="form-group text-right">
        <button id="nova_qualificacao" type="button" class="btn btn-primary btn-sm" data-toggle="modal">
            Nova Qualificação
        </button>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Tipo</th>
                <th>Nome</th>
                <th>Vulgo</th>
                <th>Nome da Mãe</th>
                <th>Telefone 1</th>
                <th>Telefone 2</th>
                <th class="text-center">Operações</th>
            </tr>
            </thead>
            <tbody id="lista_qualificacao">
            </tbody>
        </table>
    </div>

    <div id="confirm_qualificacao" class="modal fade" role="dialog" aria-labelledby="confirmDeleteQalificacao"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <span id="confirmDeleteQalificacao">Tem certeza que quer excluir esta Qualificação?</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete_qualificacao">excluir
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


    <div id="formulario_qualificacao" class="modal fade" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalLabel">Qualificação</h4>
                </div>

                <div class="modal-body">
                    <div id="form_qualificacao">
                        <fieldset>
                            <input type="hidden" id="qualificacao_id" name="qualificacao_id" value="">
                            <input type="hidden" id="pessoa_id" name="pessoa_id" value="">


                            <div class="form-group col-sm-3">
                                {{ Form::label('tipo_pessoa_id', 'Tipo de pessoa' ) }}
                                {{ Form::select('tipo_pessoa_id', $tiposPessoa, null, ['class' => 'form-control'] ) }}
                            </div>

                            <div class="form-group col-sm-9">
                                <label for="nome">Nome</label>
                                <input id="nome" name="nome" maxlength="120" type="text" class="form-control">
                            </div>


                            <div class="form-group col-sm-4">
                                <label for="apelido">Vulgo/Apelido</label>
                                <input id="apelido" name="apelido" maxlength="120" type="text" class="form-control">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="rg">RG</label>
                                <input id="rg" name="rg" maxlength="20" type="text" class="form-control">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="cpf">CPF</label>
                                <input id="cpf" name="cpf" maxlength="20" type="text" class="form-control mask_cpf">
                            </div>


                            <div class="form-group col-sm-4">
                                <label for="ocupacao">Ocupação</label>
                                <input id="ocupacao" name="ocupacao" maxlength="150" type="text" class="form-control">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="matricula">Matrícula</label>
                                <input id="matricula" name="matricula" maxlength="50" type="text" class="form-control">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="local_trabalho">Local de Trabalho</label>
                                <input id="local_trabalho" name="local_trabalho" maxlength="250" type="text"
                                       class="form-control">
                            </div>


                            <div class="form-group col-sm-2">
                                <label for="estado_civil">Estado Civil</label>
                                <select id="estado_civil" name="estado_civil" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="Solteiro(a)">Solteiro(a)</option>
                                    <option value="Casado(a)">Casado(a)</option>
                                    <option value="Amasiado(a)">Amasiado(a)</option>
                                    <option value="União Estável">União Estável</option>
                                    <option value="Separado(a)">Separado(a)</option>
                                    <option value="Divorciado(a)">Divorciado(a)</option>
                                    <option value="Viúvo(a)">Viúvo(a)</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-3">
                                <label for="nacionalidade">Nacionalidade</label>
                                <input id="nacionalidade" name="nacionalidade" maxlength="80" type="text"
                                       class="form-control">
                            </div>

                            <div class="form-group col-sm-3">
                                <label for="naturalidade">Naturalidade</label>
                                <input id="naturalidade" name="naturalidade" maxlength="80" type="text"
                                       class="form-control">
                            </div>

                            <div class="form-group col-sm-3">
                                <label for="data_nascimento">Nascimento</label>
                                <input id="data_nascimento" name="data_nascimento" maxlength="10" type="text"
                                       class="form-control mask_date">
                            </div>

                            <div class="form-group col-sm-1">
                                <label for="idade">Idade</label>
                                <input id="idade" name="idade" maxlength="3" type="text" class="form-control">
                            </div>


                            <div class="form-group col-sm-4">
                                <label for="nome_mae">Nome da Mãe</label>
                                <input id="nome_mae" name="nome_mae" maxlength="120" type="text" class="form-control">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="nome_pai">Nome do Pai</label>
                                <input id="nome_pai" name="nome_pai" maxlength="120" type="text" class="form-control">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="responsavel">Responsável</label>
                                <input id="responsavel" name="responsavel" maxlength="120" type="text"
                                       class="form-control">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="escolaridade">Escolaridade</label>
                                <select id="escolaridade" name="escolaridade" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="Fundamental - Incompleto">Fundamental - Incompleto</option>
                                    <option value="Fundamental - Completo">Fundamental - Completo</option>
                                    <option value="Médio - Incompleto">Médio - Incompleto</option>
                                    <option value="Médio - Completo">Médio - Completo</option>
                                    <option value="Superior - Incompleto">Superior - Incompleto</option>
                                    <option value="Superior - Completo">Superior - Completo</option>
                                    <option value="Pós-graduação - Incompleto">Pós-graduação - Incompleto</option>
                                    <option value="Pós-graduação - Completo">Pós-graduação - Completo</option>
                                    <option value="Mestrado - Incompleto">Mestrado - Incompleto</option>
                                    <option value="Mestrado - Completo">Mestrado - Completo</option>
                                    <option value="Doutorado - Incompleto">Doutorado - Incompleto</option>
                                    <option value="Doutorado - Completo">Doutorado - Completo</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="sexo">Sexo</label>
                                <select id="sexo" name="sexo" class="form-control">
                                    <option value="" selected>Selecione</option>
                                    <option value="Feminino">Feminino</option>
                                    <option value="Masculino">Masculino</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-2">
                                <label for="cep">CEP</label>
                                <input id="cep" name="cep" maxlength="10" type="text" class="form-control mask_cep">
                            </div>

                            <div class="form-group col-sm-2">
                                <label>Buscar CEP</label>
                                <button id="busca_cep" type="button" class="btn btn-default">Busca</button>
                            </div>

                            <div class="form-group col-sm-8">
                                <label for="logradouro">Logradouro</label>
                                <input id="logradouro" name="logradouro" maxlength="180" type="text"
                                       class="form-control">
                            </div>

                            <div class="form-group col-sm-2">
                                <label for="numero">Número</label>
                                <input id="numero" name="numero" maxlength="10" type="text" class="form-control">
                            </div>


                            <div class="form-group col-sm-5">
                                <label for="complemento">Complemento</label>
                                <input id="complemento" name="complemento" maxlength="100" type="text"
                                       class="form-control">
                            </div>

                            <div class="form-group col-sm-7">
                                <label for="referencia">Referência</label>
                                <input id="referencia" name="referencia" maxlength="150" type="text"
                                       class="form-control">
                            </div>


                            <div class="form-group col-sm-4">
                                <label for="bairro">Bairro</label>
                                <input id="bairro" name="bairro" type="text" maxlength="60" class="form-control">
                            </div>

                            <div class="form-group col-sm-4">
                                {{ Form::label('uf', 'UF' ) }}
                                {{ Form::select('uf', $estados, null, ['class' => 'form-control'] ) }}
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="cidade">Cidade</label>
                                <select id="cidade" name="cidade" class="form-control">
                                    <option value="" selected>Selecione</option>
                                </select>
                            </div>


                            <div class="form-group col-sm-4">
                                <label for="telefone_1">Telefone 1</label>
                                <input id="telefone_1" name="telefone_1" maxlength="25" type="text"
                                       class="form-control mask_phone">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="telefone_2">Telefone 2</label>
                                <input id="telefone_2" name="telefone_2" maxlength="25" type="text"
                                       class="form-control mask_phone">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="advogado">Advogado</label>
                                <input id="advogado" name="advogado" maxlength="250" type="text" class="form-control">
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="tipificacao">Tipificação</label>
                                <textarea id="tipificacao" name="tipificacao" rows="8" class="form-control"></textarea>
                            </div>

                        </fieldset>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="form-group" style="margin-top: 20px; text-align: center">
                        <div>
                            <div>
                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"
                                        id="envia_qualificacao">
                                    Criar
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

