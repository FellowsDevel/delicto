<div class="panel-body">
    <div class="table-responsive">
        <div class="form-group">
            <div class="col-sm-4 form-group">

                {{ Form::open([ 'id' => 'form_anexo', 'url' => '/attachment/'. $abertura['id'], 'enctype' =>"multipart/form-data", 'files'=> true ]) }}
                <div class="form-group">
                    <input id="anx_file" type="file" name="file[]" class="file" multiple data-show-upload="true"
                           data-show-caption="true"/>
                </div>
                <div class="form-group">
                    <input id="submit_anexo" type="submit" name="submit" value="Enviar" class="btn btn-primary"/>
                </div>

                {{ Form::close() }}

                <div id="progressbar"></div>
                <div>
                    <span id="cur_val"></span>
                    <span id="total"></span>
                    <button id="cancela_envio" class="btn btn-default">Cancela envio</button>
                </div>
            </div>

            <div class="col-sm-8 form-group">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th class="text-center">Operações</th>
                    </tr>
                    </thead>
                    <tbody id="listaAnexos"></tbody>
                </table>
            </div>

        </div>
    </div>

    <div id="mensagem_upload"></div>

    <div id="confirm_anexo" class="modal fade" role="dialog" aria-labelledby="confirmDeleteAnexo" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <span id="confirmDeleteAnexo">Tem certeza que deseja excluir este Anexo?</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete_anexo">Excluir</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

</div>
