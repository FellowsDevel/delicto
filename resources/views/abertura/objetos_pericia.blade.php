<div>
    <div class="table-responsive">
        <div class="form-group">

            <div class="form-group col-sm-4">
                <div class="form-group">
                    <label for="tipo_objeto_id">Tipo de Objeto</label>
                    {{ Form::select('tipo_objeto_id', $tiposObjeto, null, ['id' => 'tipo_objeto_id', 'class' => 'form-control'] ) }}
                </div>
                <div class="form-group">
                    <label for="lista_objetos_a_periciar">Lista de Objetos</label>
                    <select id="lista_objetos_a_periciar" class="form-control" multiple name="objetos_a_periciar[]"
                            size="5"></select>
                </div>
                <button style="margin-top: 10px;" id="associar_objeto_pericia" type="button"
                        class="btn btn-primary btn-sm">
                    Associar Tipo ao Objeto
                </button>
            </div>

            <div class="form-group col-sm-8">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Tipo de Objeto</th>
                        <th>Descrição do Objeto</th>
                        <th class="text-center">Operações</th>
                    </tr>
                    </thead>
                    <tbody id="lista_objetos_em_pericia">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

