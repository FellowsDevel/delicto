<div>
    <div class="form-group text-right">
        <button id="nova_ouvida" type="button" class="btn btn-primary btn-sm" data-toggle="modal">
            Nova Ouvida
        </button>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Data</th>
                <th>Tipo</th>
                <th>Nome</th>
                <th>Ouvida</th>
                <th class="text-center">Operações</th>
            </tr>
            </thead>
            <tbody id="lista_ouvida">
            </tbody>
        </table>
    </div>

    <div id="confirm_ouvida" class="modal fade" role="dialog" aria-labelledby="confirmDeleteOuvida" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <span id="confirmDeleteOuvida">Tem certeza que quer excluir esta Ouvida?</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete_ouvida">excluir
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


    <div id="formulario_ouvida" class="modal fade" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalLabel">Ouvida</h4>
                </div>

                <div class="modal-body">
                    <div id="form_ouvida">
                        <fieldset>
                            <input type="hidden" id="ouvida_id" name="ouvida_id" value="">

                            <div class="form-group col-sm-2">
                                <label for="data_ouvida">Data</label>
                                <input id="data_ouvida" name="data_ouvida" class="form-control mask_date"/>
                            </div>

                            <div class="form-group col-sm-10">
                                <label for="idQualificacaoOuvida">Tipo - Nome</label>
                                <select id="idQualificacaoOuvida" name="qualificacao_id" class="form-control"></select>
                            </div>

                            <div class="form-group" style="margin-top: 20px; text-align: center">
                                <div>
                                    <button type="button" class="btn btn-primary btn-sm" id="cria_ouvida">Criar</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="idOuvida">Ouvida</label>
                                <textarea rows="10" id="idOuvida" name="ouvida" class="form-control"></textarea>
                            </div>

                        </fieldset>
                    </div>
                </div>

                <div class="modal-footer">

                </div>

            </div>
        </div>
    </div>
</div>

