<div class="form-group col-sm-6">
    {{ Form::label('delegado', 'Delegado' ) }}
    {{ Form::text('delegado', null, ['class' => 'form-control', 'autofocus' => '', 'maxlength' => '120', 'required' => 'required']) }}
</div>
<div class="form-group col-sm-6">
    {{ Form::label('circunscricao', 'Circunscrição' ) }}
    {{ Form::text('circunscricao', null, ['class' => 'form-control', 'maxlength' => '120', 'required' => 'required']) }}
</div>
<div class="form-group col-sm-3">
    {{ Form::label('abertura_estado', 'UF' ) }}
    {{ Form::select('abertura_estado', $estados, null, ['class' => 'form-control'] ) }}
</div>
<div class="form-group col-sm-3">
    {{ Form::label('abertura_cidade', 'Cidade' ) }}
    @if(empty($abertura))
        {{ Form::select('abertura_cidade', $cidades, null, ['class' => 'form-control'] ) }}
    @else
        {{ Form::select('abertura_cidade', $cidades, $abertura->abertura_cidade, ['class' => 'form-control'] ) }}
    @endif
</div>
<div class="form-group col-sm-2">
    {{ Form::label('hora', 'Hora' ) }}
    {{ Form::text('hora', null,['class' => 'form-control mask_hour', 'title' => "HH:MM", 'pattern' => '([01]?[0-9]|2[0-3]):[0-5][0-9]' ]) }}
</div>
<div class="form-group col-sm-2">
    {{ Form::label('data', 'Data' ) }}
    {{ Form::text('data', null, ['class' => 'form-control mask_date']) }}
</div>
<div class="form-group col-sm-2">
    {{ Form::label('bo', 'B.O.' ) }}
    {{ Form::text('bo', null,['class' => 'form-control', 'maxlength' => '220']) }}
</div>