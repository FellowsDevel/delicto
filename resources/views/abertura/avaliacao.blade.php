<div>
    <div class="table-responsive">

        <input type="hidden" id="avaliacao_id" name="avaliacao_id" value="<?= $avaliacao["id"] ?>">

        <div class="form-group col-sm-8">
            {{ Form::label('perito_1_nome', 'Nome Primeiro Perito' ) }}
            {{ Form::text('perito_1_nome', $avaliacao[ "perito_1_nome" ] , ['class' => 'form-control']) }}
        </div>

        <div class="form-group col-sm-4">
            {{ Form::label('perito_1_matricula', 'Matrícula do Primeiro Perito' ) }}
            {{ Form::text('perito_1_matricula', $avaliacao[ "perito_1_matricula" ], ['class' => 'form-control']) }}
        </div>

        <div class="form-group col-sm-8">
            {{ Form::label('perito_2_nome', 'Nome Segundo Perito' ) }}
            {{ Form::text('perito_2_nome', $avaliacao[ "perito_2_nome" ], ['class' => 'form-control']) }}
        </div>

        <div class="form-group col-sm-4">
            {{ Form::label('perito_2_matricula', 'Matrícula do Segundo Perito' ) }}
            {{ Form::text('perito_2_matricula', $avaliacao[ "perito_2_matricula" ], ['class' => 'form-control']) }}
        </div>


        <div class="form-group col-sm-3">
            <label for="objeto_id">Objeto</label>
            <select id="objeto_id" name="objeto_id" class="form-control"></select>
        </div>

        <div class="form-group col-sm-2">
            <label for="quantidade">Quantidade</label>
            <input type="text" id="quantidade" name="quantidade" class="form-control mask_number" pattern="\d*">
        </div>

        <div class="form-group col-sm-2">
            <label for="preco">Preço Unitário</label>
            <input type="text" id="valor" name="valor" class="form-control mask_money">
        </div>

        <div class="form-group col-sm-5">
            <button type="button" id="bt_adiciona_avaliacao" class="btn btn-primary" style="margin-top: 25px;"
                    disabled="disabled">
                Adicionar
            </button>
        </div>

        <div class="form-group col-sm-12">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>Objeto</th>
                    <th>Quantidade</th>
                    <th>Preço Unitário</th>
                    <th>Total</th>
                    <th class="text-center">Operações</th>
                </tr>
                </thead>
                <tbody id="lista_objetos_avaliacao">
                </tbody>
                <tfoot id="footer_lista_objetos_avaliacao">
                </tfoot>
            </table>
        </div>
    </div>

</div>
