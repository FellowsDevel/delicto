<!DOCTYPE html>
<html>
<head>
    <title>Erro - Recurso não disponível</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #D11;
            display: table;
            font-size: 26px;
            font-weight: 700;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            color: #A00;
            font-size: 72px;
            margin-bottom: 40px;
            font-weight: 500;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Recurso não disponível.</div>
        <div>O recurso que você está tentando utilizar não existe ou você não possui privilégio para acessar.</div>
    </div>
</div>
</body>
</html>
