@extends('layouts.app')

@section('content')
    <div class="col-sm-offset-4 col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    Cadastro de Tipo de Objeto
                </strong>
            </div>

            {{ Html::ul($errors->all()) }}

            <div class="panel-body">

                {{ Form::model($tipo_objeto, array('route' => array('tipo_objeto.update', $tipo_objeto->id), 'method' => 'PUT')) }}

                @include('grupo.form', ['submit_text' => 'Atualizar'])

                {{ Form::close() }}

            </div>
        </div>
    </div>
@endsection
