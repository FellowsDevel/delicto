@extends('layouts.app')

@section('content')
    <div class="col-sm-offset-4 col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    Cadastro de Tipo de Objetos
                </strong>
            </div>

            {{ Html::ul($errors->all()) }}

            <div class="panel-body">

                {{ Form::open([ 'url' => 'tipo_objeto' ]) }}

                @include('tipo_objeto.form', ['submit_text' => 'Enviar'])

                {{ Form::close() }}

            </div>
        </div>
    </div>
@endsection
