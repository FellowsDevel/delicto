<div class="form-group">
    {{ Form::label('descricao', 'Descrição', ['class' => 'control-label']) }}
    {{ Form::text('descricao', null, ['class' => 'form-control', 'autofocus' => '', 'maxlength' => '120']) }}
</div>

{{ Form::button($submit_text, ['type' => 'submit', 'class' => 'btn btn-default']) }}
{{ Form::button('Voltar', ['class' => 'btn btn-default', 'onclick' => 'history.back()']) }}
