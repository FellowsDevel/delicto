@extends('layouts.app')

@section('content')
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    Cadastro de Função
                </strong>
            </div>

            {{ Html::ul($errors->all()) }}

            <div class="panel-body">

                {{ Form::model($funcao, array('route' => array('funcao.update', $funcao->id), 'method' => 'PUT')) }}

                @include('funcao.form', ['submit_text' => 'Atualizar'])

                {{ Form::close() }}

            </div>
        </div>
    </div>
@endsection
