@extends('layouts.stub')


<?php /* Aqui colocamos espaço para CSS pontuais das páginas que herdam esta */ ?>
@section('stub_styles')

    @yield('styles')

@endsection


<?php
/*
 * Barra superior de navegação do sistema
 */
?>
@section('stub_content')
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed"
                        data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/home') }}"><?= NOME_SISTEMA ?></a>
                <div style="float: left; height: 50px; padding: 15px 15px; font-size: 14px; line-height: 22px; color: #999;">
                    <a href="{{ url('/usuario') }}">
                        {{ Auth::user()->name }}
                    </a>
                </div>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ url('/apfd') }}">APFD</a></li>
                    <li><a href="{{ url('/portaria') }}">Portaria</a></li>
                    <li><a href="{{ url('/pecas') }}">Peças</a></li>

                    <li class="dropdown">

                        @if( \Auth::user()->verificaPermissao('tela_configuracao') )
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                               role="button" aria-haspopup="true"
                               aria-expanded="false">Configurações <span class="caret"></span></a>
                        @endif

                        <ul class="dropdown-menu">
                            <li class="dropdown-header">Cadastros</li>
                            <li><a href="{{ url('/grupo') }}">Grupos</a></li>
                            <li><a href="{{ url('/funcao') }}">Funções</a></li>
                            <li><a href="{{ url('/tipo_pessoa') }}">Tipo de Pessoa</a></li>
                            <li><a href="{{ url('/tipo_objeto') }}">Tipo de Objeto</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>

                    <li><a href="{{ url('/logout') }}">Sair</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        @yield('content')
    </div>


    <?php
    /*
     * Este DIV é para exibir mensagens diversas em qualquer tela.
     *
     * Para utilizar basta fazer a chamada na função : exibeMensagem("mensagem")
     *
     */
    ?>
    <div id="mensagens" class="modal fade" role="dialog" aria-labelledby="mensagensLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header modal-header-info">
                    <div class="text-center">
                        <span id="mensagensLabel">&nbsp;</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

@endsection


<?php /* Aqui colocamos espaço para scripts pontuais das páginas que herdam esta */ ?>
@section('stub_scripts')

    @yield('scripts')

@endsection
