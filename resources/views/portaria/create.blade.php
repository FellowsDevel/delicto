@extends('layouts.app')

@section('content')
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    Cadastro de Portaria
                </strong>
            </div>

            <div id="painel" class="panel-body" hidden>

                {{ Form::open([ 'url' => '/portaria' ]) }}

                @include('portaria.form', [ 'submit_text'   => 'Criar',
                'tiposPessoa'   => $tiposPessoa,
                'tiposObjeto'   => $tiposObjeto,
                'despacho'      => $despacho,
                'fianca'        => $fianca,
                'estados'       => $estados,
                'abertura'      => $abertura])


            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        $(function () {
            $("#tabs").tabs();
            $("#tabs").tabs("disable"); //desabilita todas as abas
            $("#tabs").tabs("enable", 0); // habilita apenas a primeira
            $("#tabs").tabs("option", "active", 0); // torna aprimeira aba ativa
            $("#painel").removeAttr('hidden');

            $.datetimepicker.setLocale('pt-BR');
            $("#data").datetimepicker({
                timepicker: false,
                format: 'd/m/Y',
                language: 'pt-BR',
                lang: 'pt-BR'
            });

            montaSelectCidade($("#abertura_estado"), $("#abertura_cidade"));

            var maskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            };
            var options = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
                }
            };

            $('.mask_date').mask('00/00/0000', {placeholder: "__/__/____"});
            $('.mask_hour').mask('00:00', {placeholder: "__:__"});
            $('.mask_cpf').mask('000.000.000-00');
            $('.mask_cep').mask('00.000-000');
            $('.mask_money').mask('000.000.000.000.000,00', {reverse: true});
            $('.mask_phone').mask(maskBehavior, options);
        });
    </script>
@endsection

