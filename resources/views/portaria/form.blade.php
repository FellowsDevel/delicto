{{ Html::ul($errors->all()) }}

<div id="tabs">
    <ul>
        <li><a href="#abertura">Abertura</a></li>
        <li><a href="#qualificacao">Qualificação</a></li>
        <li><a href="#ouvida">Ouvida</a></li>
        <li><a href="#despacho">Despacho</a></li>
        <li><a href="#tabs_objetos">Objetos</a></li>
        <li><a href="#avaliacao_indireta">Avaliação Indireta</a></li>

        <li><a href="#boletim_individual">Boletim Individual</a></li>
        <li><a href="#circunstancia">Circunstâncias do fato</a></li>
        <li><a href="#ordem_servico">Ordem de Serviço</a></li>
        <li><a href="#relatorio">Relatório</a></li>
        <li><a href="#anexos">Anexos</a></li>
    </ul>

    <div id="abertura">
        <div class="form-group">
            <fieldset>
                @include('abertura.abertura', [ 'cidades'   => $cidades,
                                                'estados'   => $estados,
                                                'abertura'  => $abertura])
                <div class="form-group col-sm-2">
                    {{ Form::label('tombamento', 'Tombamento' ) }}
                    {{ Form::text('tombamento', null,['id' => 'tombamento', 'class' => 'form-control', 'maxlength' => '60']) }}
                </div>
            </fieldset>
        </div>
        <div class="form-group" style="margin-top: 20px; text-align: center">
            <div>
                <div>
                    {{ Form::button($submit_text, ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>


    <div id="qualificacao">
        <div class="form-group">
            @include('abertura.qualificacao', [ 'tiposPessoa'   => $tiposPessoa ])
        </div>
    </div>

    <div id="ouvida">
        <div class="form-group">
            @include('abertura.ouvida')
        </div>
    </div>

    <div id="despacho">
        <div class="form-group">
            <input type="hidden" id="despacho_id" name="despacho_id" value="<?= $despacho["id"] ?>"/>
            <textarea id="id_despacho" rows="12" class="form-control">{{$despacho["despacho"]}}</textarea>
        </div>
    </div>


    <div id="tabs_objetos">
        <fieldset class="panel panel-default">
            <ul>
                <li><a href="#tab_obj_apreendidos">Apreendidos</a></li>
                <li><a href="#tab_obj_restituidos">Restituidos</a></li>
                <li><a href="#tab_obj_pericia">Perícia IC</a></li>
            </ul>
            <div id="tab_obj_apreendidos">
                <div class="form-group">
                    @include('abertura.objetos_apreendidos')
                </div>
            </div>

            <div id="tab_obj_restituidos">
                <div class="form-group">
                    @include('abertura.objetos_restituidos')
                </div>
            </div>

            <div id="tab_obj_pericia">
                <div class="form-group">
                    @include('abertura.objetos_pericia', ['tiposObjeto', $tiposObjeto])
                </div>
            </div>
        </fieldset>
    </div>


    <div id="avaliacao_indireta">
        <div class="form-group">
            @if($avaliacao)
                @include('abertura.avaliacao')
            @endif
        </div>
    </div>

    <div id="boletim_individual">
        <div class="form-group">
            @include('abertura.boletim_individual', ['cidades', $cidades])
        </div>
    </div>

    <div id="circunstancia">
        @if($circunstancia)
        <div class="form-group">
            <input type="hidden" id="circunstancia_id" name="circunstancia_id" value="<?= $circunstancia["id"] ?>"/>
            <textarea id="id_circunstancia" rows="12" class="form-control">{{$circunstancia["circunstancia"]}}</textarea>
        </div>
            @endif
    </div>

    <div id="ordem_servico">
        <div class="form-group">
            @include('abertura.ordem_servico')
        </div>
    </div>

    <div id="relatorio">
        <div class="form-group">
            <input type="hidden" id="apfdrelatorio_id" name="apfdrelatorio_id" value="<?= $apfdrelatorio["id"] ?>"/>
            <textarea id="id_apfdrelatorio" rows="12" class="form-control">{{$apfdrelatorio["relatorio"]}}</textarea>
        </div>
    </div>

    <div id="anexos">
        <div class="form-group">
            @if($abertura)
                @include('abertura.anexo', ['abertura', $abertura])
            @endif
        </div>
    </div>

</div>

