@extends('layouts.app')

@section('content')
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    Portaria: {{ $abertura->id }}
                </strong>
            </div>

            <div id="painel" class="panel-body" hidden>

                {{ Form::model($abertura, array('route' => array('portaria.update', $abertura->id), 'method' => 'PUT')) }}

                @include('portaria.form', [
                'submit_text'   => 'Salvar',
                'tiposPessoa'   => $tiposPessoa,
                'tiposObjeto'   => $tiposObjeto,
                'estados'       => $estados,
                'despacho'      => $despacho,
                'fianca'        => $fianca,
                'avaliacao'     => $avaliacao,
                'abertura'      => $abertura,
                'circunstancia' => $circunstancia,
                'apfdrelatorio' => $apfdrelatorio])
                {{ Form::close() }}


            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script src="{{ url('/') }}/js/abertura.js"></script>
    <script>
        $(document).ready(function () {

            // Inicializa o Abertura
            Abertura($, '{{ csrf_token() }}', '{{ $abertura->id }}');
        });
    </script>
@endsection