<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ $url }}/css/app.css" rel="stylesheet">
</head>
<body>

<div>
    <div class="form-group text-right">
        <button id="nova_ouvida" type="button" class="btn btn-primary btn-sm" data-toggle="modal">
            Nova Ouvida
        </button>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Data</th>
                <th>Tipo</th>
                <th>Nome</th>
                <th>Ouvida</th>
                <th class="text-center">Operações</th>
            </tr>
            </thead>
        </table>
    </div>

</div>

</body>
</html>


