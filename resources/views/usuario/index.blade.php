@extends('layouts.app')

@section('content')
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="form-group">
                    <fieldset>
                        <div class="col-sm-10">
                            <strong>
                                Dados do usuário
                            </strong>
                        </div>
                        <div class="col-sm-2 text-right">
                            {{ Form::button('Alterar senha', ['id'=>'exibe_tela', 'type' => 'button', 'class' => 'btn btn-xs btn-danger']) }}
                        </div>
                    </fieldset>
                </div>
            </div>

            {{ Html::ul($errors->all()) }}

            <div class="panel-body">

                {{ Form::model($usuario, array('route' => array('usuario.update', $usuario), 'method' => 'PUT')) }}

                <div class="form-group">
                    {{ Form::label('email', 'Email', ['class' => 'control-label']) }}
                    {{ Form::text('email', null, ['class' => 'form-control', 'disabled' => 'disabled', 'maxlength' => '120']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('name', 'Nome', ['class' => 'control-label']) }}
                    {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '120'] ) }}
                </div>

                <div class="form-group">
                    {{ Form::label('funcao_id', 'Função', ['class' => 'control-label']) }}
                    {{ Form::select('funcao_id', $funcoes, null, ['class' => 'form-control'] ) }}
                </div>


                <div class="form-group">
                    {{ Form::label('matricula', 'Matrícula', ['class' => 'control-label']) }}
                    {{ Form::text('matricula', null, ['class' => 'form-control', 'maxlength' => '120']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('telefone', 'Telefone', ['class' => 'control-label']) }}
                    {{ Form::text('telefone', null, ['class' => 'form-control mask_phone', 'maxlength' => '25']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('celular', 'Celular', ['class' => 'control-label']) }}
                    {{ Form::text('celular', null, ['class' => 'form-control mask_phone', 'maxlength' => '25']) }}
                </div>

                <div class="form-group text-center">
                    {{ Form::button('Alterar', ['type' => 'submit', 'class' => 'btn btn-default btn-primary']) }}
                    {{ Form::button('Voltar', ['class' => 'btn btn-default', 'onclick' => 'history.back()']) }}
                </div>


                {{ Form::close() }}

            </div>

            <div id="altera_senha" class="modal fade" role="dialog" aria-labelledby="alterar_senha" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <span id="alterar_senha">Alteração de senha</span>
                        </div>
                        <div class="modal-body">

                            <fieldset>

                                {{ Html::ul($errors->all()) }}

                                {{ Form::open(array('url' => 'usuario/redefine', 'method' => 'POST')) }}

                                <div class="form-group col-sm-12">
                                    <label for="senha_atual">Senha atual</label>
                                    <input id="senha_atual" maxlength="256" type="password" class="form-control"
                                           autofocus="autofocus">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="nova_senha">Nova senha</label>
                                    <input id="nova_senha" maxlength="256" type="password" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="nova_senha2">Redigite a Nova senha</label>
                                    <input id="nova_senha2" maxlength="256" type="password" class="form-control">
                                </div>
                                {{ Form::close() }}

                            </fieldset>

                        </div>

                        <div style="margin-bottom: 26px; text-align: center; !important;">
                            <button type="button" class="btn btn-danger" id="altera">Alterar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function () {

            /**
             * Máscara para validar oito ou nove dígitos no telefone
             * @param val
             * @returns {string}
             */
            var maskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            };
            var options = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
                }
            };
            $('.mask_phone').mask(maskBehavior, options);

            $('#exibe_tela').click(function () {
                $('#senha_atual').val('');
                $('#nova_senha').val('');
                $('#nova_senha2').val('');
                $("#altera_senha").modal();
                $("#altera_senha").on('shown.bs.modal', function () {
                    $('#senha_atual').focus();
                    $('#altera').attr('disabled', true);
                });
            });

            $('#senha_atual').keyup(function () {
                if ($(this).val()) {
                    $('#nova_senha, #nova_senha2').attr("disabled", false);
                } else {
                    $('#nova_senha, #nova_senha2').attr("disabled", true);
                }
            });

            $('#nova_senha, #nova_senha2').keyup(function () {
                if ($('#nova_senha').val().length > 0 &&
                        $('#nova_senha2').val().length > 0 &&
                        ( $('#nova_senha').val() === $('#nova_senha2').val() )) {
                    $('#altera').attr('disabled', false);
                } else {
                    $('#altera').attr('disabled', true);
                }
            });

            $('#nova_senha, #nova_senha2').attr("disabled", true);

            $("#altera").click(function (event) {
                event.preventDefault();

                var url = '/redefinesenha';

                var dados = {
                    _method: 'POST',
                    _token: $("input[name='_token']").val(),
                    senha_atual: $('#senha_atual').val(),
                    nova_senha: $('#nova_senha').val(),
                    nova_senha2: $('#nova_senha2').val()
                };

                enviaAjax(url, 'POST', dados,
                        function (data) {
                            if (data.success) {
                                window.location = '/';
                            }
                        },
                        function () {
                            $("#altera_senha").modal('hide');
                        })

            });
        });
    </script>

@endsection