@extends('layouts.app')

@section('content')
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="form-group" style="height: 15px;">
                    <div class="col-sm-6">
                        <strong>
                            Cadastro de APFD
                        </strong>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button id="adicionar" class="btn btn-primary btn-sm">Novo APFD</button>
                    </div>
                </div>
            </div>

            <div class="panel-body">

                @if(old('descricao'))
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        APFD de número <strong>{{ old('id') }}</strong> criado com Sucesso!
                    </div>
                @endif

                @if(empty($registros) || $registros->total() == 0)

                    <div class="alert alert-danger">
                        Não há APFD cadastrado!
                    </div>

                @else

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">

                            <thead>
                            <th>Nº APFD</th>
                            <th>Data</th>
                            <th>B.O.</th>
                            <th>Autuado(s)</th>
                            <th>Vítima(s)</th>
                            <th class="text-center">Operações</th>
                            </thead>

                            @foreach ($registros as $key => $value)
                                <tr>
                                    <td>{{ $value->id }}</td>
                                    <td>{{ $value->data }}</td>
                                    <td>{{ $value->bo }}</td>
                                    <td>
                                        <?php
                                        $qlfs = $value->qualificacoes;
                                        foreach ($qlfs as $k => $v) {
                                            if ($v->tipo_pessoa_id == '1') {
                                                echo $v->pessoa->nome . '<br />';
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        foreach ($qlfs as $k => $v) {
                                            if ($v->tipo_pessoa_id == '6') {
                                                echo $v->pessoa->nome . '<br />';
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <a class="glyphicon glyphicon-edit" title="Editar"
                                           href="{{url('/apfd/'.$value->id . '/edit')}}"></a>
                                        <a class="glyphicon glyphicon-remove deletar" title="Excluir"
                                           href="{{url('/apfd/'.$value->id)}}"
                                           data-token="{{ csrf_token() }}"></a>
                                        @if($value->habilita_impressao)
                                            <a class="glyphicon glyphicon-print imprimir" title="Imprimir"
                                               target="_blank" href="{{url('/relatorio/apfd/'.$value->id)}}"></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>


                    <div id="confirm" class="modal fade" role="dialog" aria-labelledby="confirmDeleteLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <span id="confirmDeleteLabel">Tem certeza que quer excluir este APFD?</span>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete">
                                        excluir
                                    </button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif
                {!! $registros->render() !!}
                @if(Auth::user()->id === 2)
                    <p>APFDs no sistema: {{$registros->total()}}</p>
                @else
                    <p>APFDs do usuário: {{$registros->total()}}</p>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>

        $(document).ready(function () {

            var url = '';
            var token = '';
            var id = '';

            $('#adicionar').click(function () {
                window.location = '/apfd/create';
            });

            $('#confirm').on('hidden.bs.modal', function () {
                id = '';
                url = '';
                token = '';
            });

            $('#delete').click(function () {
                if (url === '') {
                    return;
                }
                var dados = {_method: 'delete', _token: token, id: id};
                $.ajax({
                    url: url,
                    type: 'post',
                    data: dados,
                    success: function (data) {
                        if (data.error_code) {
                            exibeMensagem(data.error_msg);
                        } else {
                            window.location = '/apfd';
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 403) {
                            exibeMensagem('Você não tem permissão para excluir este APFD');
                        } else {
                            console.log('jqXHR', jqXHR);
                            console.log('textStatus', textStatus);
                            console.log('errorThrown', errorThrown);
                        }
                    }
                });

            });

            $('.deletar').click(function (event) {
                event.preventDefault();
                url = $(this).attr('href');
                token = $(this).data('token');
                id = url.substr(url.indexOf("d/") + 2);
                $('#confirm').modal();
            });
        });


    </script>

@endsection