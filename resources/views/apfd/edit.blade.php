@extends('layouts.app')

@section('content')
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    APFD: {{ $abertura->id }}
                </strong>
            </div>

            <div id="painel" class="panel-body" hidden>

                {{ Form::model($abertura, array('route' => array('apfd.update', $abertura->id), 'method' => 'PUT')) }}

                @include('apfd.form', [
                'submit_text'   => 'Salvar',
                'tiposPessoa'   => $tiposPessoa,
                'tiposObjeto'   => $tiposObjeto,
                'estados'       => $estados,
                'despacho'      => $despacho,
                'fianca'        => $fianca,
                'avaliacao'     => $avaliacao,
                'abertura'      => $abertura,
                'apfdrelatorio' => $apfdrelatorio])
                {{ Form::close() }}


            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script src="{{ url('/') }}/js/abertura.js"></script>
    <script>
        $(document).ready(function () {

            // Inicializa o Abertura
            Abertura($, '{{ csrf_token() }}', '{{ $abertura->id }}');
        });
    </script>
@endsection