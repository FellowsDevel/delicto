@extends('layouts.app')

@section('content')
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    Cadastro de Tipo de Pessoa
                </strong>
            </div>

            {{ Html::ul($errors->all()) }}

            <div class="panel-body">

                {{ Form::model($tipo_pessoa, array('route' => array('tipo_pessoa.update', $tipo_pessoa->id), 'method' => 'PUT')) }}

                @include('grupo.form', ['submit_text' => 'Atualizar'])

                {{ Form::close() }}

            </div>
        </div>
    </div>
@endsection
