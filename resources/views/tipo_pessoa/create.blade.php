@extends('layouts.app')

@section('content')
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    Cadastro de Tipo de Pessoas
                </strong>
            </div>

            {{ Html::ul($errors->all()) }}

            <div class="panel-body">

                {{ Form::open([ 'url' => 'tipo_pessoa' ]) }}

                @include('tipo_pessoa.form', ['submit_text' => 'Enviar'])

                {{ Form::close() }}

            </div>
        </div>
    </div>
@endsection
