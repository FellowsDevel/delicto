@extends('layouts.app')

@section('content')
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    Cadastro de Grupo
                </strong>
            </div>

            {{ Html::ul($errors->all()) }}

            <div class="panel-body">

                {{ Form::model($grupo, array('route' => array('grupo.update', $grupo->id), 'method' => 'PUT')) }}

                @include('grupo.form', ['submit_text' => 'Atualizar'])

                {{ Form::close() }}

            </div>
        </div>
    </div>
@endsection
