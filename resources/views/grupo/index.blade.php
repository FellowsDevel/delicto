@extends('layouts.app')

@section('content')
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    Cadastro de Grupos
                </strong>
                <button id="adicionar" class="btn btn-primary btn-sm">
                    <span class="glyphicon glyphicon-plus"></span> Novo
                </button>
            </div>

            {{ Html::ul($errors->all()) }}

            <div class="panel-body">

                @if(old('descricao'))
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>{{ old('descricao') }}</strong> criado com Sucesso!
                    </div>
                @elseif(old('alterado'))
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        Grupo <strong>{{ old('anterior') }}</strong> alterado para
                        <strong>{{ old('alterado') }}</strong> com
                        Sucesso!
                    </div>
                @endif



                @if(empty($registros) || $registros->total() == 0)

                    <div class="alert alert-danger">
                        Não há Grupo cadastrado!
                    </div>

                @else

                    <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover">

                            <thead>
                            <th>Descrição</th>
                            <th class="text-center">Operações</th>
                            </thead>

                            @foreach ($registros as $key => $value)

                                <tr>
                                    <td class="col-md-10">{{ $value->descricao }}</td>
                                    <td class="col-md-2 text-center">

                                        <a class="glyphicon glyphicon-edit" title="Editar"
                                           href="{{url('/grupo/'.$value->id . '/edit')}}"></a>
                                        <a class="glyphicon glyphicon-remove deletar" title="Excluir"
                                           href="{{url('/grupo/'.$value->id)}}"
                                           data-token="{{ csrf_token() }}"></a>

                                    </td>
                                </tr>

                            @endforeach
                        </table>
                    </div>

                    <div id="confirm" class="modal fade" role="dialog" aria-labelledby="confirmDeleteLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <span id="confirmDeleteLabel">Tem certeza que quer excluir este grupo?</span>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete">
                                        excluir
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif
                {!! $registros->render() !!}
                <p>Numero de itens: {{$registros->total()}}</p>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>

        $(document).ready(function () {

            var url = '';
            var token = '';

            $('#adicionar').click(function () {
                window.location = '/grupo/create';
            });

            $('#confirm').on('hidden.bs.modal', function () {
                url = '';
                token = '';
            });

            $('#delete').click(function () {
                if (url === '') {
                    return;
                }
                $.ajax({
                    url: url,
                    type: 'post',
                    data: {_method: 'delete', _token: token},
                    success: function (data) {
                        if (data.error_code) {
                            exibeMensagem(data.error_msg);
                        } else {
                            window.location = '/grupo';
                        }
                    }
                });
            });


            $('.deletar').click(function (event) {
                event.preventDefault();
                url = $(this).attr('href');
                token = $(this).data('token');
                $('#confirm').modal();

            });


        });
    </script>

@endsection