@extends('layouts.stub')

@section('stub_content')

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed"
                        data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/home') }}"><?= NOME_SISTEMA ?></a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ url('/login') }}">Login</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <p>algum conteúdo</p>
                <p>algum conteúdo</p>
                <p>algum conteúdo</p>
                <p>algum conteúdo</p>
                <p>algum conteúdo</p>
                <p>algum conteúdo</p>
                <p>algum conteúdo</p>
                <p>algum conteúdo</p>
            </div>
        </div>
    </div>

@endsection
