var Abertura = function ($, token, abertura_id) {

    var KEY_PRESS_DELAY = 750; //milisseconds
    var KEY_PRESS_NOW = 1; //milisseconds

    var digitando_ouvida;
    var digitando_tipificacao;
    var digitando_despacho;
    var digitando_fianca;
    var digitando_avaliacao;
    var digitando_apfdrelatorio;
    var digitando_circunstancia;


    var url = '';
    var digitando = false;

    var tabs = $("#tabs");
    var tabs_objetos = $("#tabs_objetos");

    tabs.tabs();
    tabs.tabs("enable");
    tabs.tabs("option", "active", 0);
    $("#painel").removeAttr('hidden'); // exibe o painel após renderizar as tabs

    tabs_objetos.tabs();
    tabs_objetos.tabs("enable");
    tabs_objetos.tabs("option", "active", 0);

    // $("#tabs_objetos").tabs({
    //   beforeActivate: function (event, ui) {
    //     var aba = ui.newPanel.selector;
    //
    //     if (aba === '#tab_obj_apreendidos') {
    //     } else if (aba === '#tab_obj_restituidos') {
    //     } else if (aba === '#tab_obj_pericia') {
    //     }
    //   }
    // });


    /**
     * Máscara para validar oito ou nove dígitos no telefone
     * @param val
     * @returns {string}
     */
    var maskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };
    var options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(maskBehavior.apply({}, arguments), options);
        }
    };

    $('.mask_date').mask('00/00/0000', {placeholder: "__/__/____"});
    $('.mask_hour').mask('00:00', {placeholder: "__:__"});
    $('.mask_cpf').mask('000.000.000-00');
    $('.mask_cep').mask('00.000-000');
    $('.mask_number').mask('00000');
    $('.mask_money').mask('000.000.000.000.000,00', {reverse: true});
    $('.mask_phone').mask(maskBehavior, options);


    montaSelectCidade($("#abertura_estado"), $("#abertura_cidade"));
    montaSelectCidade($("#uf"), $("#cidade"));
    montaSelectCidade($("#uf_boletim"), $("#distrito"));


    atualizarListaQualificacao();
    atualizarListaOuvida();
    atualizarListaObjeto();
    atualizarListaBoletins();
    atualizarListaOrdemServico();
    atualizarListaAnexos();


    $.datetimepicker.setLocale('pt-BR');
    $("#data, #data_boletim, #data_ouvida, #data_inquerito, #data_os").datetimepicker({
        timepicker: false,
        format: 'd/m/Y',
        language: 'pt-BR',
        lang: 'pt-BR'
    });


    // Desabilita o envio de form utilizando ENTER nos objetos INPUT e SELECT
    $('input').keydown(desabilitaEnter);
    $('select').keydown(desabilitaEnter);

    /**
     * Atualiza as tabelas que contém Objeto
     */
    function atualizaTabelas() {
        atualizarListaObjeto();
        atualizarListaObjetosRestituir();
        atualizarListaVitimasObjetosRestituir();
        atualizarListaObjetosParaPericiar();
        atualizarTabelaObjetosRestituidos();
        atualizaTabelaObjetosPericiados();

        atualizarListaObjetosDisponiveisAvaliacaoIndireta();
        atualizaTabelaObjetosAvaliados();
    }


    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ///
    /// Funcionalidades do sistema
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////
    // Tipificação - início
    //////////////////////////////////////////////
    // Quando o usuário estiver digitando, após pausa configurada no atraso, em milissegundo, salva
    function salvaTipificacao(atraso) {

        if (digitando_tipificacao) {
            clearInterval(digitando_tipificacao);
        }

        digitando_tipificacao = setTimeout(
            function () {
                var id = $("#tipificacao_id").val();
                var url = window.location.origin + '/tipificacao/' + id;
                var tipificacao = $("#id_tipificacao").val().trim();

                var dados = {
                    _method: 'POST',
                    _token: token,
                    abertura_id: abertura_id,
                    id: id,
                    tipificacao: tipificacao
                };

                enviaAjax(url, 'POST', dados, '');

            }
            , atraso);
    }

    $('#id_tipificacao').keydown(function () {
        salvaTipificacao(KEY_PRESS_DELAY);
    });

    $("#bt_salvar_tipificacao").click(function (event) {
        event.preventDefault();
        salvaTipificacao(KEY_PRESS_NOW);
    });

    //////////////////////////////////////////////
    // Tipificação - fim
    //////////////////////////////////////////////


    //////////////////////////////////////////////
    // Qualificação - início
    //////////////////////////////////////////////

    var data_nascimento = $("#data_nascimento");
    data_nascimento.datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    });

    /**
     * Quando sair do input da data de nascimento, se tiver sido preenchida, calcula a idade
     */
    data_nascimento.focusout(function (event) {
        if ($(this).val() !== '') {
            $("#idade").val(getAge($(this).val()));
        } else {
            $("#idade").val('');
        }
    });

    /*
     * Sempre que a modal de confirmação é fechada, limpamos as variáveis
     */
    $("#confirm_qualificacao").on('hidden.bs.modal', function () {
        url = '';
    });


    /*
     * Atualiza a lista de Qualificações do APFD via ajax
     */
    function atualizarListaQualificacao() {

        var url = window.location.origin + '/qualificacao/abertura/' + abertura_id;

        enviaAjax(url, 'GET', {}, function (data) {

            var content = '';
            $.each(data, function (i, item) {
                content += '<tr>';
                content += '  <td>' + item.tipo_pessoa.descricao + '</td>';
                content += '  <td>' + item.pessoa.nome + '</td>';
                content += '  <td>' + item.pessoa.apelido + '</td>';
                content += '  <td>' + item.pessoa.nome_mae + '</td>';
                content += '  <td>' + item.pessoa.telefone_1 + '</td>';
                content += '  <td>' + item.pessoa.telefone_2 + '</td>';
                content += '  <td class="text-center">';
                content += "    <span style='cursor: pointer' data-toggle='modal' class='glyphicon glyphicon-edit editar_qualificacao' title='Editar' data-qualif='" + item.id + "'></span>";
                content += "    <span style='cursor: pointer' data-toggle='modal' class='glyphicon glyphicon-remove deletar_qualificacao' title='Excluir' data-qualif='" + item.id + "' ></span>";
                content += "  </td>";
                content += '</tr>';
            });

            $("#lista_qualificacao").html(content);

        }, atualizaTabelas);

    }

    /*
     * Limpa o formulário deixando pronto para uma nova utilização
     */
    function limparFormQualificacao() {
        $("#tipo_pessoa_id").val('');
        $("#pessoa_id").val('');
        $("#nome").val('');
        $("#apelido").val('');
        $("#rg").val('');
        $("#cpf").val('');
        $("#ocupacao").val('');
        $("#matricula").val('');
        $("#local_trabalho").val('');
        $("#estado_civil").val('');
        $("#nacionalidade").val('');
        $("#naturalidade").val('');
        $("#data_nascimento").val('');
        $("#idade").val('');
        $("#nome_mae").val('');
        $("#nome_pai").val('');
        $("#responsavel").val('');
        $("#escolaridade").val('');
        $("#sexo").val('');
        $("#cep").val('');
        $("#logradouro").val('');
        $("#numero").val('');
        $("#complemento").val('');
        $("#referencia").val('');
        $("#bairro").val('');
        $("#cidade").val('');
        $("#uf").val('');
        $("#telefone_1").val('');
        $("#telefone_2").val('');
        $("#advogado").val('');
        $("#tipificacao").val('');
    }

    /*
     * Retorna um objeto JSON dos dados contidos no MODAL de Qualificação
     */
    function montarDadosQualificacao() {
        return {
            _method: 'post',
            _token: token,
            abertura_id: abertura_id,
            tipo_pessoa_id: $("#tipo_pessoa_id").val(),
            pessoa_id: $("#pessoa_id").val(),
            nome: $("#nome").val(),
            apelido: $("#apelido").val(),
            rg: $("#rg").val(),
            cpf: $("#cpf").val(),
            ocupacao: $("#ocupacao").val(),
            matricula: $("#matricula").val(),
            local_trabalho: $("#local_trabalho").val(),
            estado_civil: $("#estado_civil").val(),
            nacionalidade: $("#nacionalidade").val(),
            naturalidade: $("#naturalidade").val(),
            data_nascimento: $("#data_nascimento").val(),
            idade: $("#idade").val(),
            nome_mae: $("#nome_mae").val(),
            nome_pai: $("#nome_pai").val(),
            responsavel: $("#responsavel").val(),
            escolaridade: $("#escolaridade").val(),
            sexo: $("#sexo").val(),
            cep: $("#cep").val(),
            logradouro: $("#logradouro").val(),
            numero: $("#numero").val(),
            complemento: $("#complemento").val(),
            referencia: $("#referencia").val(),
            bairro: $("#bairro").val(),
            cidade: $("#cidade").val(),
            uf: $("#uf").val(),
            telefone_1: $("#telefone_1").val(),
            telefone_2: $("#telefone_2").val(),
            advogado: $("#advogado").val(),
            tipificacao: $("#tipificacao").val()
        };
    }


    /*
     * Faz a chamada para persistir a atualizaçao de uma Qualificaçao
     */
    function atualizarQualificacao() {
        var url = window.location.origin + '/qualificacao/' + $("#qualificacao_id").val();
        var dados = montarDadosQualificacao();

        enviaAjax(url, 'POST', dados, '', atualizarListaQualificacao);
    }

    /*
     * Faz a chamada para persistir uma nova Qualificação
     */
    function criarQualificacao() {
        var url = window.location.origin + '/qualificacao';
        var dados = montarDadosQualificacao();

        enviaAjax(url, 'POST', dados, '', atualizarListaQualificacao);
    }

    /*
     * Ao clicar no botão de Nova Qualificação, prepara o modal para a Criação
     * de uma nova Qualificação
     */
    $("#nova_qualificacao").click(function (event) {
        event.preventDefault();
        limparFormQualificacao();
        $("#envia_qualificacao").text("Criar");
        $("#formulario_qualificacao").modal();
    });

    /*
     * Como utilizamos o mesmo modal para atualizar e criar qualificações
     * devemos chamar a função correta de acordo com o texto do botão
     */
    $("#envia_qualificacao").click(function (event) {
        event.preventDefault();
        if ($(this).text() === "Criar") {
            criarQualificacao();
        } else {
            atualizarQualificacao();
        }
    });

    /*
     * Ao editar uma Qualificação, devemos buscar os dados da mesma e
     * preencher a modal com os dados.
     * Devemos também alterar o texto do botão para "Atualizar"
     */
    var lista_qualificacao = $("#lista_qualificacao");

    lista_qualificacao.on('click', '.editar_qualificacao', function (event) {
        event.preventDefault();
        var url = window.location.origin + '/qualificacao/' + $(this).data('qualif');

        enviaAjax(url, 'GET', {}, function (datas) {
            var data = datas[0];

            $("#qualificacao_id").val(data.id);
            $("#tipo_pessoa_id").val(data.tipo_pessoa_id);

            $("#pessoa_id").val(data.pessoa.id);
            $("#nome").val(data.pessoa.nome);
            $("#apelido").val(data.pessoa.apelido);
            $("#rg").val(data.pessoa.rg);
            $("#cpf").val(data.pessoa.cpf);
            $("#ocupacao").val(data.pessoa.ocupacao);
            $("#matricula").val(data.pessoa.matricula);
            $("#local_trabalho").val(data.pessoa.local_trabalho);
            $("#estado_civil").val(data.pessoa.estado_civil);
            $("#nacionalidade").val(data.pessoa.nacionalidade);
            $("#naturalidade").val(data.pessoa.naturalidade);
            $("#data_nascimento").val(data.pessoa.data_nascimento);
            $("#idade").val(data.pessoa.idade);
            $("#nome_mae").val(data.pessoa.nome_mae);
            $("#nome_pai").val(data.pessoa.nome_pai);
            $("#responsavel").val(data.pessoa.responsavel);
            $("#escolaridade").val(data.pessoa.escolaridade);
            $("#sexo").val(data.pessoa.sexo);
            $("#cep").val(data.pessoa.cep);
            $("#logradouro").val(data.pessoa.logradouro);
            $("#numero").val(data.pessoa.numero);
            $("#complemento").val(data.pessoa.complemento);
            $("#referencia").val(data.pessoa.referencia);
            $("#bairro").val(data.pessoa.bairro);
            $("#cidade").val(data.pessoa.cidade);
            $("#uf").val(data.pessoa.uf);
            $("#telefone_1").val(data.pessoa.telefone_1);
            $("#telefone_2").val(data.pessoa.telefone_2);
            $("#advogado").val(data.pessoa.advogado);
            $("#tipificacao").val(data.tipificacao);

            $("#envia_qualificacao").text("Atualizar");
            $("#formulario_qualificacao").modal();
        });

    });

    /*
     * Icone de exclusão na lista de Qualificações
     */
    lista_qualificacao.on('click', '.deletar_qualificacao', function (event) {
        event.preventDefault();
        url = window.location.origin + '/qualificacao/' + $(this).data('qualif');
        $('#confirm_qualificacao').modal();
    });

    /*
     * Botao de confirmação de exclusão dentro da modal
     */
    $('#delete_qualificacao').click(function () {
        if (url === '') {
            return;
        }
        enviaAjax(url, 'POST', {_method: 'delete', _token: token}, atualizarListaQualificacao);
    });


    $("#busca_cep").click(function () {

        var cep = $("#cep").val();

        if (cep) {
            var url = window.location.origin + "/cep/" + cep;
            $.get(url, {}, function (data) {

                var success = $($.parseHTML(data)).find(".tmptabela");
                if (success) {
                    var dd = $(success).find('tr');
                    var dados = dd[1];
                    var logradouro = $(dados).children("td").first().text();
                    var bairro = $(dados).children("td").next().first().text();
                    var td3 = $(dados).children("td").next().next().first().text();

                    var cidade = td3.substr(0, td3.indexOf('/'));
                    var estado = td3.substr(td3.indexOf('/') + 1);

                    $("#logradouro").val(logradouro);
                    $("#bairro").val(bairro);
                    $("#uf").val(estado).change();

                    setTimeout(function () {
                        $("#cidade option").filter(function () {
                            return $(this).text() == cidade;
                        }).prop("selected", true);
                    }, 3000);

                }
            }, 'html').fail(function (e) {
                console.log('Erro', e);
            });

        }
    });

    //////////////////////////////////////////////
    // Qualificação - fim
    //////////////////////////////////////////////


    //////////////////////////////////////////////
    // Ouvida - início
    //////////////////////////////////////////////

    /*
     * Sempre que a modal de confirmação é fechada, limpamos as variáveis
     */
    $('#confirm_ouvida').on('hidden.bs.modal', function () {
        url = '';
    });


    /*
     * Atualiza a lista de Qualificações do APFD via ajax
     */
    function atualizarListaOuvida() {

        var url = window.location.origin + '/ouvida/abertura/' + abertura_id;

        enviaAjax(url, 'GET', {}, function (data) {

            var content = '';

            $.each(data, function (i, item) {
                var ouvida = item.ouvida || '';

                if (ouvida && ouvida.length > 200) {
                    ouvida = ouvida.substr(0, 200) + ' ...';
                }

                content += '<tr>';
                content += '  <td>' + item.data_ouvida + '</td>';
                content += '  <td>' + item.qualificacao.tipo_pessoa.descricao + '</td>';
                content += '  <td>' + item.qualificacao.pessoa.nome + '</td>';
                content += '  <td>' + ouvida + '</td>';
                content += '  <td class="text-center"> ';
                content += "    <span style='cursor: pointer' data-toggle='modal' class='glyphicon glyphicon-edit editar_ouvida' title='Editar' data-qualif='" + item.id + "'></span>";
                content += "    <span style='cursor: pointer' data-toggle='modal' class='glyphicon glyphicon-remove deletar_ouvida' title='Excluir' data-qualif='" + item.id + "' ></span>";
                content += "  </td>";
                content += '</tr>';
            });

            $("#lista_ouvida").html(content);

        });

    }


    // Quando o usuário estiver digitando, após pausa de 0.3 segundo, salva
    $('#idOuvida').keydown(
        function () {
            if (digitando_ouvida) {
                clearInterval(digitando_ouvida);
            }

            digitando_ouvida = setTimeout(function () {
                var url = window.location.origin + '/ouvida/' + $("#ouvida_id").val();
                var dados = getOuvida();
                enviaAjax(url, 'POST', dados, '', atualizarListaOuvida);
            }, KEY_PRESS_DELAY);
        }
    );

    $("#data_ouvida, #idQualificacaoOuvida").change(function () {
        if (!$("#cria_ouvida").is(':visible')) {
            $('#idOuvida').keydown();
        }
    });

    function getOuvida() {
        return {
            _method: 'post',
            _token: token,
            abertura_id: abertura_id,
            data_ouvida: $("#data_ouvida").val(),
            qualificacao_id: $("#idQualificacaoOuvida").val(),
            ouvida: $("#idOuvida").val()
        }
    }

    /*
     * Limpa o formulário deixando pronto para uma nova utilização
     */
    function limpaFormOuvida() {
        $("#idQualificacaoOuvida").val('');
        $("#data_ouvida").val('');
        $("#idOuvida").val('');
    }

    /*
     * Faz a chamada para persistir uma nova Ouvida
     */
    function criarOuvida() {
        var url = window.location.origin + '/ouvida';
        var dados = getOuvida();

        enviaAjax(url, 'POST', dados, function (data) {
            $("#ouvida_id").val(data.id);
            $("#cria_ouvida").hide();
            $("#idQualificacaoOuvida").attr('disabled', true);
            $("#data_ouvida").attr('disabled', true);
        }, function () {
            $("#idOuvida").attr('disabled', false);
            atualizarListaOuvida();
        });
    }


    function carregaPessoas(url) {
        $("#idQualificacaoOuvida").find('option').remove();

        enviaAjax(url, 'GET', {}, function (data) {

            var content = '<option value="" selected>Selecione o Tipo e Nome da Pessoa</option>';
            $.each(data, function (i, item) {
                content += '<option value="' + item.id + '">' + item.tipo_pessoa.descricao + ' - ' + item.pessoa.nome + '</td>';
            });
            $("#idQualificacaoOuvida").append(content);

        });
    }

    function carregaTodasPessoas() {
        var url = window.location.origin + '/qualificacao/abertura/' + abertura_id;
        carregaPessoas(url);
    }

    function carregaPessoasNaoOuvidas() {
        var url = window.location.origin + '/ouvida/abertas/' + abertura_id;
        carregaPessoas(url);
    }

    /*
     * Ao clicar no botão de Nova Ouvida, prepara o modal para a Criação
     * de uma nova Ouvida
     */
    $("#nova_ouvida").click(function (event) {
        event.preventDefault();
        $("#cria_ouvida").text("Criar");
        limpaFormOuvida();
        $("#idOuvida").attr('disabled', true);
        $("#idQualificacaoOuvida").attr('disabled', false);
        $("#data_ouvida").attr('disabled', false);
        $("#cria_ouvida").show();
        carregaPessoasNaoOuvidas();
        $("#formulario_ouvida").modal();
    });

    /*
     * Como utilizamos o mesmo modal para atualizar e criar ouvidas
     * devemos chamar a função correta de acordo com o texto do botão
     */
    $("#cria_ouvida").click(function (event) {
        event.preventDefault();
        if (!$("#idQualificacaoOuvida").val()) {
            exibeMensagem("É necessário informar a Pessoa para esta Ouvida!");
            return;
        }
        if (!$("#data_ouvida").val()) {
            exibeMensagem("É necessário informar a Data para esta Ouvida!");
            return;
        }
        criarOuvida();
    });

    /*
     * Ao editar uma Ouvida, devemos buscar os dados da mesma e
     * preencher a modal com os dados.
     * Devemos também alterar o texto do botão para "Atualizar"
     */
    var lista_ouvida = $("#lista_ouvida");

    lista_ouvida.on('click', '.editar_ouvida', function (event) {
        event.preventDefault();
        var url = window.location.origin + '/ouvida/' + $(this).data('qualif');

        carregaTodasPessoas();

        enviaAjax(url, 'GET', {}, function (datas) {
            var data = datas[0];
            $("#ouvida_id").val(data.id);
            $("#idQualificacaoOuvida").attr('disabled', false);
            $("#idQualificacaoOuvida").val(data.qualificacao_id);
            $("#data_ouvida").attr('disabled', false);
            $("#data_ouvida").val(data.data_ouvida);
            $("#idOuvida").attr('disabled', false);
            $("#idOuvida").val(data.ouvida);
            $("#cria_ouvida").hide();
            $("#formulario_ouvida").modal();
        });

    });

    /*
     * Icone de exclusão na lista de Qualificações
     */
    lista_ouvida.on('click', '.deletar_ouvida', function (event) {
        event.preventDefault();
        url = window.location.origin + '/ouvida/' + $(this).data('qualif');
        $('#confirm_ouvida').modal();
    });

    /*
     * Botao de confirmação de exclusão dentro da modal
     */
    $('#delete_ouvida').click(function () {
        if (url === '') {
            return;
        }
        enviaAjax(url, 'POST', {_method: 'delete', _token: token}, '', atualizarListaOuvida);

    });


//////////////////////////////////////////////
// Ouvida - fim
//////////////////////////////////////////////


//////////////////////////////////////////////
// Despacho - início
//////////////////////////////////////////////
// Quando o usuário estiver digitando, após pausa configurada no atraso, em milissegundo, salva

    function salvaDespacho(atraso) {

        if (digitando_despacho) {
            clearInterval(digitando_despacho);
        }
        digitando_despacho = setTimeout(function () {
                var id = $("#despacho_id").val();
                var url = window.location.origin + '/despacho/' + id;
                var despacho = $("#id_despacho").val().trim();

                var dados = {
                    _method: 'POST',
                    _token: token,
                    abertura_id: abertura_id,
                    id: id,
                    despacho: despacho
                };

                enviaAjax(url, 'POST', dados, '');

            }
            , atraso);
    }

    $('#id_despacho').keydown(function () {
        salvaDespacho(KEY_PRESS_DELAY);
    });

    $("#bt_salvar_despacho").click(function (event) {
        event.preventDefault();
        salvaDespacho(KEY_PRESS_NOW);
    });

//////////////////////////////////////////////
// Despacho - fim
//////////////////////////////////////////////


//////////////////////////////////////////////
// Objetos Apreendidos - início
//////////////////////////////////////////////

    function enviarObjeto() {
        var descricao = $('#idDescricaoObjeto').val().trim();

        var dado = {
            _method: 'post',
            _token: token,
            abertura_id: abertura_id,
            descricao: descricao
        };

        enviaAjax(window.location.origin + '/objeto', 'POST', dado, '', function () {
            limpaFormObjeto();
            atualizaTabelas();
        });
    }


    /**
     * se clicar no botao de envio dos objetos
     */
    $("#novo_objeto").click(function () {
        enviarObjeto();
    });


    /**
     * Insere os objetos
     */
    $('#idDescricaoObjeto').keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode && parseInt(keycode) === 13) {
            event.preventDefault();
            enviarObjeto();
        }
    });


    /**
     * Atualiza a lista de Objetos do APFD via ajax
     *
     * Esta funcionalidade também faz a edição do input quando clica 2x no TD da descrição do objeto
     *
     * Somente será persistido se após a alteração o usuário pressione Enter. Se o usuário sair da caixa de edição,
     * o valor original permanecerá
     */
    function atualizarListaObjeto() {

        url = window.location.origin + '/objeto/abertura/' + abertura_id;

        enviaAjax(url, 'GET', {}, function (data) {
            var content = '';
            $.each(data, function (i, item) {
                content += '<tr>';
                content += '  <td title="Clique duplo para editar a descrição deste Objeto e pressione Enter para gravar as alterações." class="descricao">' + item.descricao + '</td>';
                content += '  <td class="text-center"> ';
                content += "     <span style='cursor: pointer' data-toggle='modal' class='glyphicon glyphicon-edit editar_objeto' title='Editar' data-qualif='" + item.id + "'></span>";
                content += "     <span style='cursor: pointer' data-toggle='modal' class='glyphicon glyphicon-remove deletar_objeto' title='Excluir' data-qualif='" + item.id + "' ></span>";
                content += '  </td>';
                content += '</tr>';
            });

            $("#lista_objetos").html(content);

            $(".descricao").dblclick(function () {

                if ($("#idDescricao").length) {
                    return;
                }

                var self = $(this);
                var text = $(this).text();
                self.text('');
                self.append('<input class="form-control" name="descricao" id="idDescricao" value="' + text + '" >');

                $("#idDescricao").select();
                $("#idDescricao").focus();

                // clicar fora do item
                $("#idDescricao").focusout(function () {
                    $("#idDescricao").remove();
                    self.text(text);
                    atualizaTabelas();
                });

                $("#idDescricao").keydown(function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);

                    if (keycode && parseInt(keycode) === 13) { // ENTER key
                        event.preventDefault();
                        var dado = {
                            _method: 'post',
                            _token: token,
                            abertura_id: abertura_id,
                            descricao: $(this).val().trim()
                        };
                        
                        var id = $(this).parent().parent().children('td').next().children('span').data('qualif');
                        enviaAjax(window.location.origin + '/objeto/' + id, 'POST', dado, function () {
                                $("#idDescricao").remove();
                                self.text(dado.descricao);
                            }
                            , function () {
                                atualizaTabelas();
                            });

                    } else if (keycode == '27') { // ESC key
                        $("#idDescricao").focusout();
                    }
                });


            });
        });

    }

    /*
     * Limpa o formulário deixando pronto para uma nova utilização
     */
    function limpaFormObjeto() {
        $("#idDescricaoObjeto").val('');
    }

    var lista_objetos = $("#lista_objetos");
    lista_objetos.on('click', '.deletar_objeto', function (event) {
        event.preventDefault();
        url = window.location.origin + '/objeto/' + $(this).data('qualif');
        enviaAjax(url, 'POST', {_method: 'delete', _token: token}, function () {
            atualizaTabelas();
        });

    });

    lista_objetos.on('click', '.editar_objeto', function (event) {
        event.preventDefault();
        $(this).parent().parent().children('td').dblclick();
    });

//////////////////////////////////////////////
// Objetos Apreendidos - fim
//////////////////////////////////////////////


//////////////////////////////////////////////
// Objetos Restituídos - início
//////////////////////////////////////////////


    function atualizarListaVitimasObjetosRestituir() {
        var url = window.location.origin + '/restituido/vitimas/' + abertura_id;

        enviaAjax(url, 'GET', {}, function (data) {
            var sel = $('#lista_vitimas');
            sel.find('option').remove();
            sel.append($('<option>', {
                value: '',
                text: ' Selecione uma pessoa '
            }));
            $.each(data, function (i, item) {
                sel.append($('<option>', {
                    value: item.id,
                    text: item.pessoa.nome
                }));
            });
            ordenaSelect(sel);
        });
    }

    /*
     * Atualiza a lista de Objetos a Restituir do APFD via ajax
     */
    function atualizarListaObjetosRestituir() {
        var url = window.location.origin + '/restituido/restante/' + abertura_id;

        enviaAjax(url, 'GET', {}, function (data) {
            var sel = $('#lista_objetos_a_restituir');
            sel.find('option').remove();
            $.each(data, function (i, item) {
                sel.append($('<option>', {
                    value: item.id,
                    text: item.descricao
                }));
            });
            ordenaSelect(sel);
        });
    }

    /*
     * Atualiza a lista de Objetos a Restituir do APFD via ajax
     */
    function atualizarTabelaObjetosRestituidos() {

        var url = window.location.origin + '/restituido/abertura/' + abertura_id;

        enviaAjax(url, 'GET', {}, function (data) {
            var content = '';
            $.each(data, function (i, item) {
                content += '<tr>';
                content += '  <td>' + item.qualificacao.pessoa.nome + '</td>';
                content += '  <td data-value="' + item.objeto_id + '">' + item.objeto.descricao + '</td>';
                content += '  <td class="text-center">';
                content += "    <span style='cursor: pointer' class='glyphicon glyphicon-remove desassociar' title='Excluir' data-qualif='" + item.id + "' ></span>";
                content += "  </td>";
                content += '</tr>';
            });

            $("#lista_objetos_restituidos").html(content);

        });
    }


    $("#associar_objetos").click(function (event) {
        event.preventDefault();

        var url = window.location.origin + '/restituido';
        var objetos = $("#lista_objetos_a_restituir").val();
        var content = $("#lista_objetos_restituidos").html();

        if (!$('#lista_vitimas').val()) {
            return;
        }
        $.each(objetos, function (key, item) {
            var data = {
                _method: 'post',
                _token: token,
                abertura_id: abertura_id,
                objeto_id: item,
                qualificacao_id: $('#lista_vitimas').val()
            };

            var vitima = $("#lista_vitimas option:selected").text();
            var objeto = $("#lista_objetos_a_restituir option[value='" + item + "']").text();
            enviaAjax(url, 'POST', data, function (data) {
                content += '<tr>';
                content += '  <td>' + vitima + '</td>';
                content += '  <td data-value="' + item + '">' + objeto + '</td>';
                content += '  <td class="text-center">';
                content += "    <span style='cursor: pointer' class='glyphicon glyphicon-remove desassociar' title='Excluir' data-qualif='" + data.id + "' ></span>";
                content += "  </td>";
                content += '</tr>';
            }, function () {
                $("#lista_objetos_restituidos").html(content);
                $("#lista_objetos_a_restituir option[value='" + item + "']").remove();
                atualizarListaObjetosParaPericiar();
            });
        });
    });


    $("#lista_objetos_restituidos").on('click', '.desassociar', function (event) {
        event.preventDefault();

        var sel = $('#lista_objetos_a_restituir');
        sel.append($('<option>', {
            value: $(this).parent().parent().children('td').next().data('value'), //$(this).data('qualif'),
            text: $(this).parent().parent().children('td').next().text()
        }));

        ordenaSelect(sel);

        url = window.location.origin + '/restituido/' + $(this).data('qualif');
        var data = {
            _method: 'delete',
            _token: token
        };
        enviaAjax(url, 'POST', data, '', function () {
            atualizarListaObjetosParaPericiar();
        });

        $(this).parent().parent().remove();
    });


//////////////////////////////////////////////
// Objetos Restituídos - fim
//////////////////////////////////////////////


//////////////////////////////////////////////
// Objetos de Pericia - início
//////////////////////////////////////////////

    /*
     * Atualiza a lista de Objetos a Periciar do APFD via ajax
     */
    function atualizarListaObjetosParaPericiar() {
        var url = window.location.origin + '/pericia/restante/' + abertura_id;

        enviaAjax(url, 'GET', {}, function (data) {
            var sel = $('#lista_objetos_a_periciar');
            sel.find('option').remove();
            $.each(data, function (i, item) {
                sel.append($('<option>', {
                    value: item.id,
                    text: item.descricao
                }));
            });
            ordenaSelect(sel);
        });
    }

    /*
     * Atualiza a lista de Objetos Periciados do APFD via ajax
     */
    function atualizaTabelaObjetosPericiados() {

        var url = window.location.origin + '/pericia/abertura/' + abertura_id;

        enviaAjax(url, 'GET', {}, function (data) {
            var content = '';

            $.each(data, function (i, item) {
                content += '<tr>';
                content += '  <td>' + item.tipo_objeto.descricao + '</td>';
                content += '  <td data-value="' + item.objeto.id + '">' + item.objeto.descricao + '</td>';
                content += '  <td class="text-center">';
                content += "    <span style='cursor: pointer' class='glyphicon glyphicon-remove desassociar' title='Excluir' data-qualif='" + item.id + "' ></span>";
                content += "  </td>";
                content += '</tr>';
            });

            $("#lista_objetos_em_pericia").html(content);

        });
    }


    /**
     * Adiciona titulo para a escolha do tipo de objeto (Pericia IC)
     */
    (function () {

        $("#tipo_objeto_id").prepend("<option value='' selected='selected'>Selecione o tipo </option>");

    }());

    $("#associar_objeto_pericia").click(function (event) {
        event.preventDefault();

        var url = window.location.origin + '/pericia';
        var objetos = $("#lista_objetos_a_periciar").val();
        var content = $("#lista_objetos_em_pericia").html();

        if (!$('#tipo_objeto_id').val()) {
            return;
        }

        $.each(objetos, function (key, item) {
            var data = {
                _method: 'post',
                _token: token,
                abertura_id: abertura_id,
                objeto_id: item,
                tipo_objeto_id: $('#tipo_objeto_id').val()
            };

            var tipoObjeto = $("#tipo_objeto_id option:selected").text();
            var objeto = $("#lista_objetos_a_periciar option[value='" + item + "']").text();

            enviaAjax(url, 'POST', data, function (data) {
                content += '<tr>';
                content += '  <td>' + tipoObjeto + '</td>';
                content += '  <td data-value="' + item + '">' + objeto + '</td>';
                content += '  <td class="text-center">';
                content += "    <span style='cursor: pointer' class='glyphicon glyphicon-remove desassociar' title='Excluir' data-qualif='" + data.id + "' ></span>";
                content += "  </td>";
                content += '</tr>';
            }, function () {
                $("#lista_objetos_em_pericia").html(content);
                $("#lista_objetos_a_periciar option[value='" + item + "']").remove();
                atualizarListaObjetosRestituir();
            });
        }, function () {
            atualizarListaObjetosRestituir();
        });
    });

    $("#lista_objetos_em_pericia").on('click', '.desassociar', function (event) {
        event.preventDefault();

        var sel = $('#lista_objetos_a_periciar');
        sel.append($('<option>', {
            value: $(this).parent().parent().children('td').next().data('value'),
            text: $(this).parent().parent().children('td').next().text()
        }));

        ordenaSelect(sel);

        url = window.location.origin + '/pericia/' + $(this).data('qualif');
        var data = {
            _method: 'delete',
            _token: token
        };
        enviaAjax(url, 'POST', data, '', function () {
            atualizarListaObjetosRestituir();
        });

        $(this).parent().parent().remove();
    });


//////////////////////////////////////////////
// Objetos de Pericia  - fim
//////////////////////////////////////////////


//////////////////////////////////////////////
// Avaliação indireta - início
//////////////////////////////////////////////

    function salvarAvaliacao(atraso) {
        if (digitando_avaliacao) {
            clearInterval(digitando_avaliacao);
        }
        digitando_avaliacao = setTimeout(function () {
                var url = window.location.origin + '/avaliacao/' + $('#avaliacao_id').val();

                var dados = {
                    _method: 'POST',
                    _token: token,
                    abertura_id: abertura_id,
                    perito_1_nome: $('#perito_1_nome').val(),
                    perito_1_matricula: $('#perito_1_matricula').val(),
                    perito_2_nome: $('#perito_2_nome').val(),
                    perito_2_matricula: $('#perito_2_matricula').val()
                };

                enviaAjax(url, 'POST', dados, '');

            }
            , atraso);
    }

    $('#perito_1_nome, #perito_1_matricula, #perito_2_nome, #perito_2_matricula').keydown(function () {
        salvarAvaliacao(KEY_PRESS_DELAY);
    });

    $("#objeto_id").change(habilitarBotaoAdicionar);
    $("#quantidade, #valor").keydown(habilitarBotaoAdicionar);

    function habilitarBotaoAdicionar() {
        $('#bt_adiciona_avaliacao').attr('disabled', !dadosPreenchidos());
    }

    function dadosPreenchidos() {
        return ($('#objeto_id').val() &&
        $('#quantidade').val() &&
        $('#valor').val() &&
        $('#avaliacao_id').val());
    }


    function atualizarListaObjetosDisponiveisAvaliacaoIndireta() {
        url = window.location.origin + '/avaliacao/restante/' + abertura_id;
        enviaAjax(url, 'GET', {}, function (data) {
            var sel = $('#objeto_id');
            sel.find('option').remove();

            $.each(data, function (i, item) {
                sel.append($('<option>', {
                    value: item.id,
                    text: item.descricao
                }));
            });
            ordenaSelect(sel);
            sel.prepend("<option value='' selected='selected'>Selecione um objeto </option>");
        });
    }


    $('#bt_adiciona_avaliacao').click(function (event) {
        event.preventDefault();
        adicionarObjetoAvaliacao();
    });


    function montaLinhaTabela(objeto) {
        var content = '';
        content += '<tr>';
        content += '  <td data-obj="' + objeto.objeto_id + '">' + objeto.objeto.descricao + '</td>';
        content += '  <td>' + objeto.quantidade + '</td>';
        content += '  <td>' + formatMoney(objeto.valor, 2, "R$ ", ".", ",") + '</td>';
        content += '  <td>' + formatMoney((objeto.valor * objeto.quantidade), 2, "R$ ", ".", ",") + '</td>';
        content += '  <td class="text-center">';
        content += "    <span style='cursor: pointer' class='glyphicon glyphicon-remove desassociar' title='Excluir' data-qualif='" + objeto.id + "' ></span>";
        content += "  </td>";
        content += '</tr>';
        return content;
    }


    $("#lista_objetos_avaliacao").on('click', '.desassociar', function (event) {
        event.preventDefault();

        url = window.location.origin + '/avaliacao/' + abertura_id + '/' + $(this).data('qualif');
        var data = {
            _method: 'delete',
            _token: token
        };
        enviaAjax(url, 'POST', data, '', function () {
            atualizarListaObjetosDisponiveisAvaliacaoIndireta();
            atualizaTabelaObjetosAvaliados();
        });

        $(this).parent().parent().remove();
    });


    function adicionarObjetoAvaliacao() {

        var url = window.location.origin + '/avaliacao/' + abertura_id + '/objeto';

        var dados = {
            _method: 'POST',
            _token: token,
            abertura_id: abertura_id,
            avaliacao_indireta_id: $('#avaliacao_id').val(),
            objeto_id: $('#objeto_id').val(),
            quantidade: $('#quantidade').val(),
            valor: parseFloat(unformatMoney($('#valor').val()))
        }


        enviaAjax(url, 'POST', dados, function (data) {
            var valorTotal = $('#total_id');
            var content = '';
            var total = parseFloat(unformatMoney(valorTotal.text()));

            if (data.valor) {
                total += parseFloat(data.valor) * parseFloat(data.quantidade);
            }

            content = montaLinhaTabela(data);
            $("#objeto_id option[value='" + data.objeto_id + "']").remove();

            valorTotal.text('Total geral: ' + formatMoney(parseFloat(total), 2, "R$ ", ".", ","));

            $("#lista_objetos_avaliacao").append(content);

            $('#objeto_id').val('');
            $('#quantidade').val('');
            $('#valor').val('');
            habilitarBotaoAdicionar();

        });
    }


    function atualizaTabelaObjetosAvaliados() {
        var url = window.location.origin + '/avaliacao/' + abertura_id;

        enviaAjax(url, 'GET', {}, function (data) {
            var content = '';
            var total = 0.0;

            $.each(data, function (i, item) {

                if (item.valor) {
                    total += parseFloat(item.valor) * parseFloat(item.quantidade);
                }
                content += montaLinhaTabela(item);
            });

            var footer = '';
            footer += '<tr>';
            footer += '  <td colspan="5"><strong><span id="total_id">Total geral: ' + formatMoney(total, 2, "R$ ", ".", ",") + '</span></strong></td>';
            footer += '</tr>';
            $("#footer_lista_objetos_avaliacao").html(footer);

            $("#lista_objetos_avaliacao").html(content);

        });
    }


//////////////////////////////////////////////
// Avaliação indireta - fim
//////////////////////////////////////////////


//////////////////////////////////////////////
// Fiança - início
//////////////////////////////////////////////
// Quando o usuário estiver digitando, após pausa configurada no atraso, em milissegundo, salva
    function salvaFianca(atraso) {
        if (digitando_fianca) {
            clearInterval(digitando_fianca);
        }
        digitando_fianca = setTimeout(function () {
                var id = $("#fianca_id").val();
                var url = window.location.origin + '/fianca/' + id;
                var fianca = $("#id_fianca").val().trim();

                var dados = {
                    _method: 'POST',
                    _token: token,
                    abertura_id: abertura_id,
                    id: id,
                    fianca: fianca
                };

                enviaAjax(url, 'POST', dados, '');

            }
            , atraso);
    }

    $('#id_fianca').keydown(function () {
        salvaFianca(KEY_PRESS_DELAY);
    });

//////////////////////////////////////////////
// Fiança - fim
//////////////////////////////////////////////


//////////////////////////////////////////////
// Boletim individual - início
//////////////////////////////////////////////


    $("#confirm_boletim_individual").on('hidden.bs.modal', function () {
        url = '';
    });


    function selectBoletinsRestante() {
        var url = window.location.origin + '/boletim/restante/' + abertura_id;
        enviaAjax(url, 'GET', {}, function (data) {

            var sel = $('#boletim_qualificacao_id');
            sel.find('option').remove();

            $.each(data, function (i, item) {
                sel.append($('<option>', {
                    value: item.id,
                    text: item.pessoa.nome
                }));
            });
            ordenaSelect(sel);
            sel.prepend("<option value='' selected='selected'>Selecione um Autuado</option>");

        });
    }


    $("#novo_boletim_individual").click(function (event) {
        event.preventDefault();
        limparFormBoletim();
        $("#envia_boletim_individual").text("Criar");
        $("#formulario_boletim_individual").modal();
    });


    $("#data_boletim").datetimepicker({
        timepicker: false,
        format: 'd/m/Y',
        language: 'pt-BR',
        lang: 'pt-BR'
    });

    function atualizarListaBoletins() {

        var url = window.location.origin + '/boletim/abertura/' + abertura_id;

        enviaAjax(url, 'GET', {}, function (data) {

            var content = '';
            $.each(data, function (i, item) {
                content += '<tr>';
                content += '  <td>' + item.qualificacao.pessoa.nome + '</td>';
                content += '  <td>' + item.data + '</td>';
                content += '  <td>' + item.periodo + '</td>';
                content += '  <td>' + item.praticado + '</td>';
                content += '  <td class="text-center">';
                content += "    <span style='cursor: pointer' data-toggle='modal' class='glyphicon glyphicon-edit editar_boletim' title='Editar' data-qualif='" + item.id + "'></span>";
                content += "    <span style='cursor: pointer' data-toggle='modal' class='glyphicon glyphicon-remove deletar_boletim' title='Excluir' data-qualif='" + item.id + "' ></span>";
                content += "  </td>";
                content += '</tr>';
            });
            $("#lista_boletim_individual").html(content);
        });
    }

    function limparFormBoletim() {
        $("#id").val('');
        $("#boletim_qualificacao_id").val('');
        $("#uf_boletim").val('');
        $("#distrito").val('');
        $("#zona").val('');
        $("#data_boletim").val('');
        $("#periodo").val('');
        $("#praticado").val('');
        $("#lugar_ocorrencia").val('');
        $("#meio_empregado").val('');
        $("#motivos_presumiveis").val('');
        $("#infracao_prevista").val('');
        $("#preso").val('');
        $("#data_inquerito").val('');
    }

    function montarDadosBoletim() {
        return {
            _method: 'post',
            _token: token,
            abertura_id: abertura_id,
            qualificacao_id: $("#boletim_qualificacao_id").val(),
            uf_boletim: $("#uf_boletim").val(),
            distrito: $("#distrito").val(),
            zona: $("#zona").val(),
            data: $("#data_boletim").val(),
            periodo: $("#periodo").val(),
            praticado: $("#praticado").val(),
            lugar_ocorrencia: $("#lugar_ocorrencia").val(),
            meio_empregado: $("#meio_empregado").val(),
            motivos_presumiveis: $("#motivos_presumiveis").val(),
            infracao_prevista: $("#infracao_prevista").val(),
            preso: $("#preso").val(),
            data_inquerito: $("#data_inquerito").val()

        };
    }

    function atualizarBoletim() {
        var url = window.location.origin + '/boletim/' + $("#id").val();
        var dados = montarDadosBoletim();
        console.log('atualizar', dados);
        enviaAjax(url, 'POST', dados, '', atualizarListaBoletins);
    }

    function criarBoletim() {
        var url = window.location.origin + '/boletim';
        var dados = montarDadosBoletim();

        enviaAjax(url, 'POST', dados, '', atualizarListaBoletins);
    }

    $("#novo_boletim_individual").click(function (event) {
        event.preventDefault();
        limparFormBoletim();
        selectBoletinsRestante();
        $("#envia_boletim_individual").text("Criar");
        $("#formulario_boletim_individual").modal();
    });

    $("#envia_boletim_individual").click(function (event) {
        event.preventDefault();
        if ($(this).text() === "Criar") {
            criarBoletim();
        } else {
            atualizarBoletim();
        }
    });


    var lista_boletins = $("#lista_boletim_individual");

    lista_boletins.on('click', '.editar_boletim', function (event) {
        event.preventDefault();
        var url = window.location.origin + '/boletim/' + $(this).data('qualif');

        selectBoletinsRestante();

        enviaAjax(url, 'GET', {}, function (datas) {
            var data = datas[0];


            var url = window.location.origin + '/boletim/restante/' + abertura_id;

            enviaAjax(url, 'GET', {}, function (resto) {

                var sel = $('#boletim_qualificacao_id');
                sel.find('option').remove();

                $.each(resto, function (i, item) {
                    sel.append($('<option>', {
                        value: item.id,
                        text: item.pessoa.nome
                    }));
                });

                sel.append($('<option>', {
                    value: data.qualificacao_id,
                    text: data.qualificacao.pessoa.nome
                }));
                ordenaSelect(sel);
                sel.val(data.qualificacao_id);
            });


            $("#id").val(data.id);

            $("#uf_boletim").val(data.uf_boletim);
            $("#distrito").val(data.distrito);
            $("#zona").val(data.zona);
            $("#data_boletim").val(data.data);
            $("#periodo").val(data.periodo);
            $("#praticado").val(data.praticado);
            $("#lugar_ocorrencia").val(data.lugar_ocorrencia);
            $("#meio_empregado").val(data.meio_empregado);
            $("#motivos_presumiveis").val(data.motivos_presumiveis);
            $("#infracao_prevista").val(data.infracao_prevista);
            $("#preso").val(data.preso);
            $("#data_inquerito").val(data.data_inquerito);

            $("#envia_boletim_individual").text("Atualizar");
            $("#formulario_boletim_individual").modal();
        });

    });

    /*
     * Icone de exclusão na lista de Qualificações
     */
    lista_boletins.on('click', '.deletar_boletim', function (event) {
        event.preventDefault();
        url = window.location.origin + '/boletim/' + $(this).data('qualif');
        $('#confirm_boletim_individual').modal();
    });

    /*
     * Botao de confirmação de exclusão dentro da modal
     */
    $('#delete_boletim_individual').click(function () {
        if (url === '') {
            return;
        }
        enviaAjax(url, 'POST', {_method: 'delete', _token: token}, atualizarListaBoletins);
    });


//////////////////////////////////////////////
// Boletim individual - fim
//////////////////////////////////////////////


//////////////////////////////////////////////
// Ordem Serviço - início
//////////////////////////////////////////////

    /*
     * Sempre que a modal de confirmação é fechada, limpamos as variáveis
     */
    $('#confirm_os').on('hidden.bs.modal', function () {
        url = '';
    });

    function atualizarListaOrdemServico() {
        var url = window.location.origin + '/os/' + abertura_id;
        enviaAjax(url, 'GET', {}, function (data) {
            var content = '';
            $.each(data, function (i, item) {
                content += montaLinhaTabelaOs(item);
            });
            $("#lista_os").html(content);
        });
    }


    function montaLinhaTabelaOs(objeto) {
        var content = '';
        var dili = objeto.diligencia;

        if (dili && dili.length > 40) {
            dili = dili.substr(0, 40) + ' ...';
        }

        content += '<tr>';
        content += '  <td>' + objeto.sequencial + '</td>';
        content += '  <td>' + objeto.data_os + '</td>';
        content += '  <td>' + dili + '</td>';
        content += '  <td class="text-center">';
        content += "    <span style='cursor: pointer' data-toggle='modal' class='glyphicon glyphicon-edit editar' title='Editar' data-qualif='" + objeto.id + "'></span>";
        content += "    <span style='cursor: pointer' data-toggle='modal' class='glyphicon glyphicon-remove deletar' title='Excluir' data-qualif='" + objeto.id + "' ></span>";
        content += "  </td>";
        content += '</tr>';
        return content;
    }

    $("#sequencial, #data_os, #diligencia").keydown(habilitarBotaoAdicionarOS);

    function habilitarBotaoAdicionarOS() {
        $('#cria_os').attr('disabled', !dadosOsPreenchidos());
    }

    function dadosOsPreenchidos() {
        return (
            $('#sequencial').val() &&
            $('#data_os').val() &&
            $('#diligencia').val()
        );
    }

    function getDadosOS() {
        return {
            _method: 'POST',
            _token: token,
            abertura_id: abertura_id,
            sequencial: $('#sequencial').val(),
            data_os: $('#data_os').val(),
            diligencia: $('#diligencia').val()
        };
    }


    function limpaFormOS() {
        $('#sequencial').val('');
        $('#data_os').val('');
        $('#diligencia').val('');
    }

    function criarOS() {
        var url = window.location.origin + '/os';
        var dados = getDadosOS();

        enviaAjax(url, 'POST', dados, function (data) {
            $("#lista_os").append(montaLinhaTabelaOs(data));
            limpaFormOS();
        });
    }

    function atualizarOS() {
        var url = window.location.origin + '/os/' + $("#os_id").val() + '/edit';
        var dados = getDadosOS();

        enviaAjax(url, 'POST', dados, '', atualizarListaOrdemServico);
    }

    /*
     * Ao clicar no botão de Nova Ouvida, prepara o modal para a Criação
     * de uma nova Ouvida
     */
    $("#nova_os").click(function (event) {
        event.preventDefault();
        $("#cria_os").text("Criar");
        $('#cria_os').attr('disabled', true);
        limpaFormOS();
        $('#sequencial').attr('disabled', false);
        $('#data_os').attr('disabled', false);
        $('#diligencia').attr('disabled', false);
        $("#cria_os").show();
        $("#formulario_os").modal();
    });

    /*
     * Como utilizamos o mesmo modal para atualizar e criar ouvidas
     * devemos chamar a função correta de acordo com o texto do botão
     */
    $("#cria_os").click(function (event) {
        event.preventDefault();
        if (!dadosOsPreenchidos()) {
            exibeMensagem("É necessário preencher todos os campos");
            return;
        }
        if ($(this).text() === "Criar") {
            criarOS();
        } else {
            atualizarOS();
        }
    });


    $("#sequencial, #data_os, #diligencia").change(function () {
        if (!$("#cria_os").is(':visible')) {
            atualizarOS();
        }
    });


    var lista_os = $("#lista_os");

    lista_os.on('click', '.editar', function (event) {
        event.preventDefault();
        var url = window.location.origin + '/os/' + $(this).data('qualif') + '/edit';

        enviaAjax(url, 'GET', {}, function (datas) {
            var data = datas[0];
            $("#os_id").val(data.id);

            $("#sequencial").attr('disabled', false);
            $("#sequencial").val(data.sequencial);

            $("#data_os").attr('disabled', false);
            $("#data_os").val(data.data_os);

            $("#diligencia").attr('disabled', false);
            $("#diligencia").val(data.diligencia);

            $("#cria_os").text("Atualizar");
            $("#cria_os").hide();

            $("#formulario_os").modal();
        });

    });

    /*
     * Icone de exclusão na lista de Qualificações
     */
    lista_os.on('click', '.deletar', function (event) {
        event.preventDefault();
        url = window.location.origin + '/os/' + $(this).data('qualif');
        $('#confirm_os').modal();
    });

    /*
     * Botao de confirmação de exclusão dentro da modal
     */
    $('#delete_os').click(function () {
        if (url === '') {
            return;
        }
        enviaAjax(url, 'POST', {_method: 'delete', _token: token},
            '', atualizarListaOrdemServico);

    });


//////////////////////////////////////////////
// Ordem Serviço - fim
//////////////////////////////////////////////


//////////////////////////////////////////////
// ApfdRelatório - início
//////////////////////////////////////////////
// Quando o usuário estiver digitando, após pausa configurada no atraso, em milissegundo, salva
    function salvaApfdRelatorio(atraso) {
        if (digitando_apfdrelatorio) {
            clearInterval(digitando_apfdrelatorio);
        }
        digitando_apfdrelatorio = setTimeout(
            function () {
                var id = $("#apfdrelatorio_id").val();
                var url = window.location.origin + '/apfdrelatorio/' + id;
                var apfdrelatorio = $("#id_apfdrelatorio").val().trim();

                var dados = {
                    _method: 'POST',
                    _token: token,
                    abertura_id: abertura_id,
                    id: id,
                    relatorio: apfdrelatorio
                };

                enviaAjax(url, 'POST', dados, '');

            }
            , atraso);
    }

    $('#id_apfdrelatorio').keydown(function () {
        salvaApfdRelatorio(KEY_PRESS_DELAY);
    });

//////////////////////////////////////////////
// ApfdRelatório - fim
//////////////////////////////////////////////

//////////////////////////////////////////////
// Anexos - início
//////////////////////////////////////////////

    function montaLinhaAnexo(objeto) {
        var content = '';
        content += '<tr>';
        content += '  <td><a target="_blank" href="/attachment/' + objeto.id + '">' + objeto.name + '</a><br></td>';
        content += '  <td class="text-center">';
        content += "    <span style='cursor: pointer' class='glyphicon glyphicon-remove excluir' title='Excluir' data-qualif='" + objeto.id + "' ></span>";
        content += "  </td>";
        content += '</tr>';
        return content;
    }

    function atualizarListaAnexos() {
        enviaAjax(window.location.origin + '/attachments/' + abertura_id, 'GET', {}, function (data) {
            var str = '';
            $.each(data, function (idx, value) {
                str += montaLinhaAnexo(value);
            });
            $("#listaAnexos").append(str);
        });
    }

    $("#listaAnexos").on('click', '.excluir', function (event) {
        event.preventDefault();
        url = window.location.origin + '/attachment/' + $(this).data('qualif');
        $('#confirm_anexo').modal();
    });

    $('#delete_anexo').click(function () {
        if (url === '') {
            return;
        }
        enviaAjax(url, 'POST', {_method: 'delete', _token: token},
            function () {
                $("#listaAnexos").html('');
                atualizarListaAnexos();
            });
    });


    function montaDadosEnvio() {
        var form_data = new FormData();
        var filedata = document.getElementsByName("file[]");
        form_data.append("_token", token);
        form_data.append("_method", 'post');
        form_data.append("abertura_id", abertura_id);
        form_data.append("file", filedata[0].files);
        console.log("arquivos", filedata[0].files);
        return form_data;
    }

    $("#cancela_envio").hide();

    var requ;
    $("#form_anexo").on("submit", function (event) {
        event.preventDefault();
        $("#submit_anexo").attr('disabled', true);

        var formData = new FormData(this);

        function limpa() {
            $("#cur_val").text('');
            $("#total").text('');
            $("#cancela_envio").hide();
            $("#progressbar").progressbar('destroy');
            $("#submit_anexo").attr('disabled', false);
        }

        $("#cancela_envio").show();

        requ = $.ajax({
            url: window.location.origin + '/attachment/' + abertura_id,
            type: 'POST',
            data: formData,
            success: function (data) {
                $("#listaAnexos").html('');
                atualizarListaAnexos();
                limpa();
                $("#anx_file").val('');
            },
            cache: false,
            contentType: false,
            processData: false,
            xhr: function () {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                    myXhr.upload.addEventListener('progress', function (evt) {
                        $("#progressbar").progressbar({
                            max: evt.total,
                            value: evt.loaded
                        });
                        $("#cur_val").text(evt.loaded);
                        $("#total").text(evt.total);
                    }, false);
                }
                return myXhr;
            }
        });

        $("#cancela_envio").click(function (e) {
            e.preventDefault();
            requ.abort();
            limpa();
        });
    });


//////////////////////////////////////////////
// Anexos - fim
//////////////////////////////////////////////


//////////////////////////////////////////////
// Circunstancias - início
//////////////////////////////////////////////
    // Quando o usuário estiver digitando, após pausa configurada no atraso, em milissegundo, salva
    function salvaCircunstancia(atraso) {
        if (digitando_circunstancia) {
            clearInterval(digitando_circunstancia);
        }
        digitando_circunstancia = setTimeout(
            function () {
                var id = $("#circunstancia_id").val();
                var url = window.location.origin + '/circunstancia/' + id;
                var circunstancia = $("#id_circunstancia").val().trim();

                var dados = {
                    _method: 'POST',
                    _token: token,
                    abertura_id: abertura_id,
                    id: id,
                    circunstancia: circunstancia
                };

                enviaAjax(url, 'POST', dados, '');

            }
            , atraso);
    }

    $('#id_circunstancia').keydown(function () {
        salvaCircunstancia(KEY_PRESS_DELAY);
    });

//////////////////////////////////////////////
// Circunstancias - fim
//////////////////////////////////////////////
}

