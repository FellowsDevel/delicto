/**
 * Função para buscar as cidades a partir do select estado
 *
 * @param estado
 * @param cidade
 */
function montaSelectCidade(estado, cidade) {
    estado.change(function () {
        var url = "/cidades/" + $(this).val();
        cidade.empty();
        cidade.append('<option value="" selected>Selecione</option>');
        $.get(url, null, function (cidades) {
            $.each(cidades, function (k, v) {
                cidade.append('<option value="' + k + '">' + v + '</option>');
            });
        });
    });
}


/**
 * Ordena uma lista no select alfabeticamente pelo texto
 * @param sel
 */
function ordenaSelect(sel) {
    var opts_list = sel.find('option');
    opts_list.sort(function (a, b) {
        return $(a).text() > $(b).text() ? 1 : -1;
    });
    sel.html('').append(opts_list);
    sel.val('');
}


/**
 * Intercepta o pressionamento de uma tecla e executa uma função
 * quando a tecla for pressionada
 * @param event
 */
function desabilitaTecla(event, key) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode && parseInt(keycode) === parseInt(key)) {
        event.preventDefault();
    }
}


/**
 * Desabilita o pressionamento da tecla Enter
 * @param event
 */
function desabilitaEnter(event) {
    desabilitaTecla(event, 13);
}


/**
 * Esta função exibe mensagens através de uma Modal localizada na view principal da aplicação
 *
 * @param {String} msg
 * @returns
 */
function exibeMensagem(msg) {
    $('#mensagensLabel').text(msg);
    $('#mensagens').modal();
}

function getAge(dateString) {
    var now = new Date();
    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    if (dateString.substring(2, 3) === '-' ||
        dateString.substring(2, 3) === '/' ||
        dateString.substring(2, 3) === '.') {
        // DD-MM-YYYY
        var dob = new Date(dateString.substring(6, 10),
            dateString.substring(3, 5) - 1,
            dateString.substring(0, 2)
        );

    } else {
        // YYYY-MM-DD
        var dob = new Date(dateString.substring(0, 4),
            dateString.substring(5, 7) - 1,
            dateString.substring(8, 10)
        );
    }
    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow < dateDob) {
        monthAge--;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    return yearAge;
}

/**
 * Função para realizar chamada Ajax com funções no sucesso e na finalização.
 * No sucesso, é possível chamar a função passando como parâmetro a resposta do Ajax
 * Na finalização é possível executar uma função sem parâmetros
 *
 * @param  url - url a ser acessada
 * @param  type - tipo da chamada: GET, POST
 * @param  dados - objeto JSON ex.: { a: 'abc' }
 * @param  cb_sucess - função a ser executada com os addos retornados da chamada
 * @param  cb_done - função a ser executado após o recebimento e processamento
 */
function enviaAjax(url, type, dados, cb_sucess, cb_done) {

    console.log('url:', url);
    console.log('dados:', dados);

    $.ajax({
        url: url,
        type: type,
        data: dados,
        success: function (data) {
            if (data.error_code) {
                exibeMensagem(data.error_msg);
            } else {
                if (cb_sucess) {
                    cb_sucess(data);
                }
                console.log("retorno do envio: ", data);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log('Erro no envio');
            console.log('xhr: ', xhr);
            console.log('textStatus: ', textStatus);
            console.log('errorThrown: ', errorThrown);
        }
    }).complete(function () { //done(function () {
        if (cb_done) {
            cb_done();
        }
    });
}

function unformatMoney(number) {
    return number.replace(/[^0-9-,]/g, '').replace(/[,]/g, '.')
}

// http://www.josscrowcroft.com/2011/code/format-unformat-money-currency-javascript/
// To set it up as a global function:
function formatMoney(number, places, symbol, thousand, decimal) {
    number = number || 0;
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}